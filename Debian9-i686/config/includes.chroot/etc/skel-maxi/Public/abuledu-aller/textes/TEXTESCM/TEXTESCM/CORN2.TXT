Ces moulins-l�, voyez-vous, faisaient la joie et la richesse de notre pays. 
Malheureusement, des Fran�ais de Paris eurent l'id�e d'�tablir une minoterie � vapeur, sur la route de Tarascon. Tout beau, tout nouveau ! Les gens prirent l'habitude d'envoyer leurs bl�s aux minotiers, et les pauvres moulins � vent rest�rent sans ouvrage. Pendant quelque temps ils essay�rent de lutter, mais la vapeur fut la plus forte et, l'un apr�s l'autre, p�ca�re! Ils furent tous oblig�s de fermer... On ne vit plus venir les petits �nes... Les belles meuni�res vendirent leurs croix d'or... Plus de muscat! Plus de farandole !... Le mistral avait beau souffler, les ails restaient 
immobiles... Puis, un beau jour, la commune fit jeter toutes ces masures � bas, et l'on sema � leur place de la vigne et des oliviers. 
Pourtant, au milieu de la d�b�cle, un moulin avait tenu bon et continuait de virer courageusement sur sa butte, � la barbe des minotiers. C'�tait le moulin de ma�tre Cornille, celui-l� m�me o� nous sommes en train de faire la veill�e en ce moment. 
(Extrait de: Le secret de ma�tre Cornille d'Alphonse Daudet)

