Kai-to, l'�l�phant. 

Kai-to est un jeune �l�phant. mais il ne barrit pas comme les autres �l�phants. Lui, il chante. 
Un jour, le chef des �l�phants avait particuli�rement nettoy� ses grandes oreilles. 
Alors, il entendit lui aussi le chant de Kai-to. 
� N'est-il pas beau, ce chant ? � demand�rent les jeunes. 
Mais le chef refusa d'�couter d'avantage. 
� Jamais, jamais encore, aucun �l�phant n'a chant� �, dit-il. �Jamais ! Donc, c'est d�fendu ! �
Il chassa Kai-to. Et quand un �l�phant est chass� du troupeau, il n'a plus le droit d'y revenir. 
Les �l�phants continu�rent leur trajet sur la route des �l�phants. Ils mangeaient et buvaient et pataugeaient et se baignaient. 
Kai-to suivait leur piste . IL se trouvait bien seul maintenant. Parfois il chantait. 
Son chant �tait souvent plein de tristesse et de col�re. 
Mais c'�tait toujours le chant de Kai-to. 
� C'est Kai-to qui chante �, disaient entre eux les jeunes �l�phants. Et ils commen�aient � s'agiter. 
Ils agitaient leurs oreilles et dressaient leur trompe contre le chef du troupeau. 
Les vieux ne disait rien, comme s'ils ne voyaient pas ce qui se passait. Ils fermaient les yeux et faisaient la sourde oreille. 

