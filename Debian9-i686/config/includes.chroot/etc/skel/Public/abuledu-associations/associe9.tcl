############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : fichier.php
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: associe9.tcl,v 1.4 2006/05/21 10:15:29 david Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
#########################################################################
#!/bin/sh
#associe10.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

###########################################################
#font2 : police de caract�re
#nb : nombre total d'item du jeu en cours
#listdata : donn�es du jeu, dans une liste
#bgl, bgn : couleurs utilis�es
#listeval :liste pour collecter les informations pour les fiches bilan
#categorie : variable pour la categorie
#user : variable pour le nom de l'utilisateur

#variables
source fonts.tcl
source path.tcl
source msg.tcl
source eval.tcl


global bgn bgl nb font2 listeval categorie user listdata Home repbasecat indexp baseHome serie arg scoretotal
variable repertcat
set c .frame.c
set bgn #ffff80
set bgl #ff80c0
set catedefaut ""
set font2 ""
set son 1
set listdata ""
set indexp 0
set score 0
set scoretotal 100

set arg [lindex $argv 0]
set serie [lindex $argv 1]

set ident $tcl_platform(user)
set plateforme $tcl_platform(platform)
initlog $plateforme $ident
inithome

#interface
. configure -background #ffff80 
frame .frame -width 640 -height 420 -background #ffff80
pack .frame -side top -fill both -expand yes
wm geometry . +0+0
canvas $c -width 640 -height 420 -background $bgn -highlightbackground #ffff80
pack $c
frame .bframe -background #ffff80
pack .bframe -side bottom
  label .bframe.lab1 -background #ffff80 -width 4
  grid .bframe.lab1 -column 0 -row 1 -sticky e

label .bframe.lab22 -background #ffff80 -text [mc {Place les bons nuages sur le soleil.}]
grid .bframe.lab22 -column 1 -padx 10 -row 1
button .bframe.b2 -image [image create photo imagbut -file [file join sysdata again2.gif] ] -background #ff80c0
grid .bframe.b2 -column 2  -padx 10 -row 1 -sticky w
button .bframe.b3 -image [image create photo suitebut -file [file join sysdata suite.gif]] -background #ff80c0 -command "quitte"
grid .bframe.b3 -column 3 -padx 1  -row 1 -sticky w


#R�cup�ration donn�es g�n�rales dans associations.conf
proc init {c} {
global bgn bgl nb font2 listeval categorie user listdata Home repbasecat indexp getcat catedefaut listeval baseHome serie iwish
set font2 ""
set son 1
set listdata ""
set indexp 0

.bframe.b2 configure -command "recommence $c"
#.bframe.b3 configure -command "suite $c"

catch {set f [open [file join $baseHome reglages associations.conf] "r"]
set catedefaut [gets $f]
set font2 [gets $f]
set son [gets $f]
set listdata [gets $f]
set listdata2 [gets $f]
set repbasecat [gets $f]
close $f}


if {$font2 == "none" || $font2 == ""} {
set font2 {Arial 20 bold}

} else {
set font2 \173$font2\175\04020\040bold
}

set listdata ""
if {$catedefaut == "none" || $catedefaut == ""} {
opencat
catch {destroy .opencate}
set ext .cat
set file $getcat$ext
 } else {
set file $catedefaut
}

wm title . "[mc {Discrimination visuelle, phoneme ou mot - }]$file" 

set categorie [lindex [split [lindex [split $file /] end] .] 0]
set ext .di2
catch {
set f [open [file join $Home categorie $repbasecat $categorie$ext] "r"]
while {![eof $f]} {
set listdata [read $f 10000]
}
close $f
}
set listdata \173$listdata\175
set listdata [string map {\n \}\040\{} $listdata]
set listdata [string map {\{\} ""} $listdata]

if {$listdata == "none" || $listdata == ""} {
tk_messageBox -message [mc {Pas de donnees valides pour ce jeu.}] -type ok -icon info 
exec $iwish associations.tcl &
exit
}
set listeval \173[mc {Discrimination visuelle, phoneme ou mot - }]\175\040$categorie

#$c create text 200 200 -text "[lindex [lindex $listdata 0] 0] [llength [lindex $listdata 0]  ]"
}


#proc�dures
####################################################""
proc recommence {c} {
global listeval user indexp categorie
#lappend listeval "4 \173[mc {Exercice recommence}]\175"
set listeval \173[mc {Discrimination visuelle, phoneme ou mot - }]\175\040$categorie
set indexp 0
place $c
}

proc suite {c} {
global catedefaut
enregistreval
set catedefaut ""
init $c
}

proc place {c} {
#nbreu index de l'item en cours
#nbessai nombre de tentatives sur l'item en cours
#nb : nombre total d'item du jeu en cours
#bonnereponse : variable pour ... la bonne r�ponse

global bgn bgl nbreu font2 nb nbessai listeval categorie bonnereponse listdata indexp score
set nbessai 0
set score 0

#set tmp [mc {Discrimination visuelle, phoneme ou mot -}]
#set listeval \173$tmp\175\040$categorie
set nb 0
image create photo pbien -file [file join sysdata pbien.gif] 
image create photo ppass -file [file join sysdata ppass.gif]
image create photo pmal -file [file join sysdata pmal.gif]
image create photo pneutre -file [file join sysdata pneutre.gif]
.bframe.lab1 configure -image pneutre -width 30
set nbreu 1
set font1 {Helvetica 12}
$c delete all
set bonnereponse [lindex [lindex $listdata $indexp] 0]
set sourcepos {{100 50} {100 130} {100 210} {250 50} {250 130} {250 210} {400 50} {400 130} {400 210} {550 50} {550 130} {550 210}}
set imgpos {{550 50} {550 100} {550 150} {550 200} {550 250} {550 300} {550 350} {550 400}}
for {set i 1} {$i <= 12} {incr i 1} {
    image create photo kimage$i -file [file join sysdata nuage.gif] -width 170 -height 100
    set xpos [lindex [lindex $sourcepos [expr $i -1]] 0]
    set ypos [lindex [lindex $sourcepos [expr $i-1]] 1]
    $c create image $xpos $ypos -image kimage$i -tags source$i
    set tmp [expr int(rand()*  [llength [lindex $listdata $indexp]])]
       $c create text $xpos $ypos -text [lindex [lindex $listdata $indexp] $tmp] -tags etiq$i -font $font2
        if {$tmp == 0} {
        incr nb
        $c addtag juste withtag source$i
        }
    $c addtag drag withtag source$i
}

if {$nb <= 2} {
   for {set i 1} {$i <= [expr 3 - $nb]} {incr i 1} {
   set tmp [expr int(rand()*13)]
     if {[lsearch [$c gettags source$i] juste] == -1} {
     $c itemconfigure source$i -tags source$i\040juste\040drag
     $c itemconfigure etiq$i -text $bonnereponse
     incr nb
     }
   }
}

set xpos 320
set ypos 380
image create photo imagecible -file [file join sysdata soleil.gif] -width 110 -height 110
$c create image $xpos $ypos -image imagecible -tags cible1
$c create text $xpos $ypos -text $bonnereponse -tags cibleetiq -font $font2
}


#appel de la proc�dure principale
init $c
place $c

#gestion des �v�nements
$c bind drag <ButtonRelease-1> "itemStopDrag $c %x %y"
$c bind drag <1> "itemStartDrag $c %x %y"
$c bind drag <B1-Motion> "itemDrag $c %x %y"
#bind . <Destroy> "quitte"


#Gestion du jeu
proc itemStartDrag {c x y} {
    global lastX lastY sourcecoord
    set sourcecoord [$c coords current]
    set lastX [$c canvasx $x]
    set lastY [$c canvasy $y]
    set coord [$c bbox current]
    $c raise current    
    set source [lindex [$c gettags current] 0]
    $c addtag moving withtag $source
    $c addtag moving enclosed [lindex $coord 0] [lindex $coord 1] [lindex $coord 2] [lindex $coord 3]
      foreach i [$c find enclosed [lindex $coord 0] [lindex $coord 1] [lindex $coord 2] [lindex $coord 3]] {    
         if {[lsearch -regexp [$c gettags $i] etiq*] != -1} {
        $c raise $i
        }
      }
  }

proc itemStopDrag {c x y} {
global sourcecoord nbreu nb font2 nbessai listeval bonnereponse score indexp listdata serie repbasecat categorie scoretotal
variable repert
#d�termination du tag de l'�tiquette que l'on a d�plac�, des coordonn�es de l'�tiquette, des coordonn�es de la cible
set strcible cible1
set tmp juste
set ciblecoord [$c coords [lindex [$c find withtag $strcible] 0]]
set coord [$c bbox current]
# test pour savoir si c'est ok
foreach i [$c find overlapping [lindex $coord 0] [lindex $coord 1] [lindex $coord 2] [lindex $coord 3]] {    
   if {[lsearch [$c gettags $i] $strcible] != -1 && [lsearch [$c gettags current] $tmp] != -1} {
        #bell
        foreach i [$c find withtag moving] {
        $c coords $i [lindex $ciblecoord 0] [lindex $ciblecoord 1]
        }
# si c'est ok et que le jeu est fini                
         if {$nbreu == $nb} {
            $c raise [$c find withtag cible1]
            $c raise [$c find withtag cibleetiq]
		incr score 10
            if {$nbessai < 2} {
            $c create text 480 400 -text [mc {Bravo!!}] -font {Arial 24}
		}
#on met � jour la fiche bilan
            switch $nbessai {
                0 {.bframe.lab1 configure -image pbien -width 30
                lappend listeval 1\040\173[lindex $listdata $indexp]\040$nbessai\040[mc {erreur(s)}]\175
                }
                1 {.bframe.lab1 configure -image ppass -width 30
                lappend listeval 2\040\173[lindex $listdata $indexp]\040$nbessai\040[mc {erreur(s)}]\175
                }
                default {.bframe.lab1 configure -image pmal -width 30
                lappend listeval 3\040\173[lindex $listdata $indexp]\040$nbessai\040[mc {erreur(s)}]\175
                }
             }
#le jeu est fini
	      #enregistreval
            update
            after 2000
            $c delete all
            set score [expr $score*10/$nb]
            set pourcent %
		if {$nbessai <2} {
		set err [mc {erreur}]
		} else {
		set err [mc {erreurs}]
		}
            $c create text 200 100 -text "$nbessai\040$err" -font {Arial 24}
		if {$score == 100} { 
            update
            after 2000
            incr indexp
            if {$indexp < [llength $listdata]} {
		place $c
		} else {
		$c create text 200 200 -text [mc {C'est fini!}] -font {Arial 24}
		set scoretotal [expr $scoretotal/[expr [llength $listdata] - $indexp +1]]
		lappend listeval \040bilan\040$serie\0405\040$repbasecat\040$repert\040$scoretotal
		enregistreval
		}
		} else {
            $c create text 200 200 -text [mc {C'est fini!}] -font {Arial 24}
            set scoretotal [expr $scoretotal/[expr [llength $listdata] - $indexp]]
		lappend listeval \040bilan\040$serie\0405\040$repbasecat\040$repert\040$scoretotal
		enregistreval
           }
           return
           } 
#le jeu n'est pas fini,on fait passer le nuage derri�re le soleil
          $c raise [$c find withtag cible1]
          $c raise [$c find withtag cibleetiq]
	    if {$nbessai == 0} {incr score 10}
          update
          after 1000
          foreach i [$c find withtag moving] {
          $c coords $i [lindex $ciblecoord 0] [lindex $ciblecoord 1]
          }
          incr nbreu
          $c delete -withtag moving
	    #set nbessai 0
          return
          } else {
#sinon, si c'est faux, on renvoie l'image � sa place

          foreach i [$c find withtag moving] {
          $c coords $i [lindex $sourcecoord 0] [lindex $sourcecoord 1]
          $c dtag $i moving
          }
          incr nbessai
	    if {$score >= 10} {incr score [expr -10]}
	    if {$scoretotal >= 1} {incr scoretotal [expr -10]}
          return
      }
   }
}

proc itemDrag {c x y} {
    catch {$c delete withtag figure}
    global lastX lastY
    set x [$c canvasx $x]
    set y [$c canvasy $y]
    $c move moving [expr $x-$lastX] [expr $y-$lastY]
    set lastX $x
    set lastY $y
}
























