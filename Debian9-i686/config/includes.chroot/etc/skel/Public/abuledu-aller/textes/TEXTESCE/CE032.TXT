La ch�vre de Monsieur Seguin (3).
Et Blanquette r�pondit : 
� - Oui, monsieur Seguin. 
- Est-ce que l'herbe te manque ici ? 
- Oh ! Non ! monsieur Seguin. 
- Tu es peut-�tre attach�e de trop court, veux-tu que j'allonge la corde ? 
- Ce n'est pas la peine, monsieur Seguin.
- Alors, qu'est-ce qu'il te faut ? Qu'est-ce que tu veux ? 
Alphonse Daudet.