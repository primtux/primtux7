Le cousin S�bastien.
Ah ! Comme il �tait dr�le, le petit cousin S�bastien ! La campagne, vraiment, il n'y connaissait rien de rien, mais ce n'�tait pas de sa faute : il vivait � la ville. Sa fa�on de s'�tonner de tout faisait pouffer de rire Marie-Blanche et Coraline.
- Oh! s'�criait-il, par exemple. Venez voir, venez voir! 
Les deux petites filles accouraient en se bousculant.
- Que se passe-t-il? demandaient-elles.
- Il y a l� un ruban de velours qui se prom�ne. Ce S�bastien !
- C'est une chenille, gros b�ta ! r�pondait Marie-Blanche. 
Dans son habit noir et brun, tranquillement, sans se presser, la chenille traversait le chemin. S�bastien s'extasiait.
- Une chenille ! Une chenille !
Ce qui surprit le plus S�bastien, ce fut le potager. Quel dr�le d'endroit, en v�rit� !... Pour lui, les tas de carottes, les montagnes de choux, toutes les salades poussaient chez le marchand de l�gumes entre la caisse enregistreuse et les paquets de figues !