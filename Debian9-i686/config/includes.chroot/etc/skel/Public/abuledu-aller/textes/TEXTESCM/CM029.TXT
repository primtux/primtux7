Le secret de ma�tre Cornille (15).
Le moulin �tait grand ouvert... Devant la porte, ma�tre Cornille, assis sur un sac de pl�tre, pleurait, la t�te dans ses mains. Il venait de s'apercevoir, en rentrant, que pendant son absence on avait p�n�tr� chez lui et surpris son triste secret. 
� Pauvre de moi ! disait-il. Maintenant, je n'ai plus qu'� mourir... Le moulin est d�shonor�. �
Et il sanglotait � fendre l'�me, appelant son moulin par toutes sortes de noms, lui parlant comme � une personne v�ritable.
Alphonse Daudet.