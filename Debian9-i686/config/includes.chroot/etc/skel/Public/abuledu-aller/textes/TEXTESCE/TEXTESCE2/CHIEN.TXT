Un gros vilain chien.

Dans un petit salon, un chien dormait en rond dans une corbeille rouge. Une chatte de la rue, le vit par la fen�tre qui �tait ouverte.
� Qu'il a de la chance, ce gros vilain chien, d'�tre l� si bien! �
Et elle miaula si fort, tant elle �tait jalouse, qu'elle r�veilla le chien.
�Mes r�ves sont partis et mon sommeil aussi, alors grogna le chien ; c'est cette horrible b�te et ses ignobles cris qui me les ont pris.�
Il se leva d'un bond et, par la porte, il sortit. Le panier resta vide... mais la chatte s'en alla aussi. Tout le long des trottoirs, sous un ciel nuageux, le chien marchait � l'aise ; mais la pluie commen�a, une goutte le mouilla, il n'aimait pas �a, il s'en retourna. En se d�p�chant il pensait : s�rement la chatte par ce temps sera rentr�e chez elle.
Mais il ne savait pas qu'elle vivait dans la maison. Ni qu'elle �tait la m�re de cinq petits chatons.
Il ne savait pas qu'elle les avait appel�s... Et quand il arriva, mouill� et tout  haletant dans le petit salon, ils �taient six dans son panier qui le regardaient. Le chien fut bien contrari�, mais il n'osa pas d�ranger tous ces jolis b�b�s. Et sur le sol, il s'est couch� � c�t�. C'est ainsi qu'une chatte et un chien, un jour, s'�taient tromp�s. Ce n'�tait pas un gros vilain chien, mais un bon gros chien. Ce n'�tait pas une horrible chatte, mais une gentille maman.
