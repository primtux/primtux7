La ch�vre de Monsieur Seguin (4). 
- Je veux aller dans la montagne, monsieur Seguin.
- Mais, malheureuse, tu ne sais pas qu'il y a le loup dans la montagne... Que feras-tu quand il viendra ?... 
- Je lui donnerai des coups de corne, monsieur Seguin. 
- Le loup se moque bien de tes cornes. Il m'a mang� des biques autrement encorn�es que toi... Tu sais bien, la pauvre vieille Renaude qui �tait ici l'an dernier ? Une ma�tresse ch�vre, forte et m�chante comme un bouc. Elle s'est battue avec le loup toute la nuit... puis, le matin, le loup l'a mang�e.
Alphonse Daudet.