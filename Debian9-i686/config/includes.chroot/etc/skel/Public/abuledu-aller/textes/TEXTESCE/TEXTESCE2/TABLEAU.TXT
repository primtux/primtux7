Je ne veux pas aller au tableau. 

Aujourdhui, c'est jeudi et j'ai mal au ventre. 
- Tu as mang� trop de chocolat, me dit maman. 
Mais moi, je sais bien que le chocolat ne me donne pas mal au ventre seulement le jeudi. 
Papa pense que j'invente une raison de rester � la maison au lieu d'aller � l'�cole, parce que je suis paresseux. Moi, je veux bien �tre courageux, mais je n'y peux rien : mon ventre ne l'est pas. 
Mes parents sont contents quand ils trouvent tout seuls des explications parce que, comme �a, ils se croient tr�s grands. Mais s'ils me le demandaient, je pourrais leur expliquer ce que mon ventre veut dire. En fait, c'est le jeudi que la ma�tresse envoie un �l�ve au tableau pour corriger les math�matiques et moi, j'ai tr�s peur d'aller au tableau. Et quand j'ai peur, je ne sais m�me plus compter. 
Je ne peux pas en parler � mes copains, ils se moqueraient de moi ! Je suis s�rement le seul � avoir peur et j'ai honte. 
Je ne peux non plus en parler � la ma�tresse : elle me dirait que je n'ai pas bien appris mes tables d'addition. 
Pourtant je les ai revues avec mon grand fr�re. Rien qu'en pensant � ma copine Pauline qui r�cite par coeur quand elle va au tableau, je me sens tellement nul que mon ventre 
est encore plus malade.
Dans le bus qui nous emm�ne � l'�cole, tout le monde rit et parle ; mais moi, je n'arrive pas � penser � autre chose qu'� ce maudit tableau. 
Je m'installe dans la classe et le supplice commnence. La ma�tresse regarde tous les �l�ves et cherche une victime.
