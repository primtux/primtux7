#!/bin/sh
#closure.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    : 
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 
# 
#  *************************************************************************
global sysFont nbreu essais auto couleur aide longchamp xcol ycol listevariable categorie startdirect user Homeconf repertoire Home initrep baseHome lecture_mot lecture_mot_cache tablecture_mot tablecture_mot_cache

source menus.tcl
source parser.tcl
source eval.tcl
source fonts.tcl
source path.tcl
source msg.tcl
source compagnon.tcl

proc cancelkey {A} {
set A ""
#focus .
}

#variables
#nbreu : nombre d'items effectues
#essais : total des essais effectues
#auto : flag de detection du mode de fonctionnement 
#couleur : couleur associ�e � l'exercice
#aide : pr�cise � quel moment doit intervenir l'aide
#longchamp : pr�cise si les champs sont de longueur variable

set nbreu 0
set essais 0
set auto 0
set couleur blue
set aide 2
set longchamp 1
set xcol 0
set ycol 0
set listevariable 1
set categorie ""
set startdirect 1
set filuser [lindex $argv 1]
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
if {$plateforme == "unix"} {set ident $env(USER)}
initlog $plateforme $ident
inithome

#interface
. configure -background black -width 640 -height 480
wm geometry . +52+0


#wm title . Bilan\040[lindex [split [lindex [split $file /] end] .] 0]
frame .menu -height 40
pack .menu -side bottom -fill both

button .menu.b1 -image [image create photo final -file [file join sysdata debut.gif]] -command "main .text"
pack .menu.b1 -side right
button .menu.bb1 -image [image create photo speak -file [file join sysdata speak.gif]] -command "litout"
pack .menu.bb1 -side right


wm geometry .wtux +200+220

set what [mc {Clique la ou il manque un signe de ponctuation, complete avec le signe convenable et appuie sur entree. Clique sur la bouee pour abandonner et obtenir la correction.}]

set what "[format [mc {Bonjour %1$s .}] [string map {.log \040} [file tail $user]]] $what"
tux_exo $what

text .text -yscrollcommand ".scroll set" -setgrid true -width 55 -height 17 -wrap word -background white -font $sysFont(l)
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both

#recup�ration des options de reglages
catch {
set f [open [file join $baseHome reglages $filuser] "r"]
set categorie [gets $f]
set repertoire [gets $f]
set aller [gets $f]
set aller_ponctu2 [lindex $aller 11]
close $f
set aide [lindex $aller_ponctu2 0]
set longchamp [lindex $aller_ponctu2 1]
set listevariable [lindex $aller_ponctu2 2]
#set categorie [lindex $aller_ponctu2 3]
set startdirect [lindex $aller_ponctu2 4]
set lecture_mot [lindex $aller_ponctu2 5]
set lecture_mot_cache [lindex $aller_ponctu2 6]
}
set initrep [file join $Home textes $repertoire]

#chargement du texte avec d�tection du mode
set auto [charge .text [file join $Home textes $repertoire $categorie]]

#focus .text
bind .text <ButtonRelease-1> "lire"
bind .text <Any-Enter> ".text configure -cursor target"
bind .text <Any-Leave> ".text configure -cursor left_ptr"
bind . <KeyPress> "cancelkey %A"

    #on enl�ve les espace doubles ou triples
    set texte [.text get 1.0 "end - 1 chars"]
    regsub -all \040+ $texte \040 texte
    .text delete 1.0 end
    .text insert 1.0 $texte
#.text configure -state disabled
if {$auto == 1} {
set lecture_mot_cache $tablecture_mot_cache(ponctuation2)
}

wm title . "[mc {Exercice}] $categorie - [lindex [lindex $listexo 12] 1]"
label .menu.titre -text "[lindex [lindex $listexo 12] 1] - [mc {Observe}]" -justify center
pack .menu.titre -side left -fill both -expand 1

proc main {t} {
#liste principale de phrases contenant les mots sans ponctuation, liste de mots � cacher
#listessai : tableau pour tenir � jour les essais sur chaque mot
#texte : le texte initial
#longmot : longueur maximale des champs de texte

global sysFont plist listemotscaches listessai texte nbmotscaches auto longchamp longmot iwish aide user lecture_mot_cache filuser
set nbmotscaches 0
button .menu.b2 -image [image create photo corriger -file [file join sysdata corriger.gif]] -command "abandon $t"
pack .menu.b2 -side left
set what [mc {Clique la ou il manque un signe de ponctuation, complete avec le signe convenable et appuie sur entree. Clique sur la bouee pour abandonner et obtenir la correction.}]

set what "[format [mc {Bonjour %1$s .}] [string map {.log \040} [file tail $user]]] $what"
tux_exo $what

#catch {destroy .menu.b1}
#button .menu.b1 -image [image create photo final -file [file join sysdata fin.gif]] -command "fin"
#pack .menu.b1 -side right
#.menu.b1 configure -image [image create photo final -file [file join sysdata fin.gif]] -command "exec $iwish aller.tcl $filuser & ;exit"
.menu.b1 configure -image [image create photo final -file [file join sysdata fin.gif]] -command "fin"

bind .text <ButtonRelease-1> ""
bind . <KeyPress> ""
bind .text <Any-Enter> ""
bind .text <Any-Leave> ""


$t configure -state normal

    pauto $t
    
$t configure -state disabled -selectbackground white -selectforeground black
    if {$lecture_mot_cache == "0" } {catch {destroy .menu.bb1}}


}

proc pauto {t} {
global sysFont plist listemotscaches listessai texte nbmotscaches aide longchamp longmot listeaide listexo iwish filuser
set nbmotscaches 0
set listemots {}
set listemotscaches {}

    catch {destroy .menu.lab}
    catch {destroy .menu.titre}

    label .menu.lab -text [lindex [lindex $listexo 12] 2] -font $sysFont(s) -height 2 -wraplength 350
#[mc {Retrouve les signes de ponctuation effaces en cliquant dans le texte puis complete et appuie sur la touche entree pour valider.}] -font $sysFont(s) -height 2 -wraplength 350
    pack .menu.lab

#On rep�re tous les signes de ponctuation et on les marque
set cur 1.0
set tmp 1.0
    while 1 {
    set cur [$t search -regexp -count length {[.!?,;:()]} $cur end]
        if {$cur == ""} {
	  break
        }
        if {[$t index $tmp ]!=[$t index $cur ] && [$t get $cur] != "..."} {
        $t mark set cur$nbmotscaches [$t index $cur]
        incr nbmotscaches
        lappend listemotscaches [$t get $cur]
        } else {
        set listemotscaches [lreplace $listemotscaches end end [lindex $listemotscaches end][$t get $cur]]
        }
    set tmp "$cur + 1c"
    set cur "$cur + 1c"
    }

set longmot 0
    foreach mot $listemotscaches {
        if {$longmot < [string length $mot]} {
        set longmot  [string length $mot]
        }
    }

    if {$nbmotscaches == 0} {
    set answer [tk_messageBox -message [mc {Erreur de traitement ou texte trop court.}] -type ok -icon info] 
    exec $iwish aller.tcl $filuser &
    exit
    }


#on remplace les signes de ponctuation et on les marques avec le tag ent$i
    for {set i 0} {$i < [llength $listemotscaches]} {incr i 1} {           
    $t delete cur$i "cur$i + [string length [lindex $listemotscaches $i]]c"
    $t insert cur$i \040 ent$i
    $t tag configure ent$i -font $sysFont(lb)
    # on enl�ve les espaces sups
    if {[$t get "cur$i"] ==" "} {
    $t delete "cur$i"
    }
    set listessai($i) 0
    }
   # .text tag bind $tag <Any-Enter> ".text tag configure $tag $bold"

$t tag configure rouge -background red

#on marque le texte entier avec le tag a
$t tag add text 1.0 end
$t tag bind text <ButtonRelease-1> "hit $t"
}

proc hit {t} {
global sysFont longchamp longmot listemotscaches essais
set ind [lsearch [$t tag names current] "ent*"] 
if {$ind != -1} {
set nom [lindex [$t tag names current] $ind]
set num [string map {ent ""} $nom]
foreach tag [$t tag names current] {
   $t tag remove $tag current
   }
    if {$longchamp == 0} { 
        entry $t.$nom -font $sysFont(l) -width [expr [string length [lindex $listemotscaches $num]]] -bg yellow
        } else {
        entry $t.$nom -font $sysFont(l) -width [expr $longmot] -bg yellow
        }   
    $t window create current -window $t.$nom
    tux_continuebonponctuation
    bind $t.$nom <Return> "verif $num $t"
    bind $t.$nom <KeyPress> "tux_continue_ponctuation"

    focus $t.$nom
return
}

if {[$t get current] == " "} {
$t tag add rouge current
incr essais
tux_echoue1
return
}
bell
}

proc verif {i t} {
global sysFont listemotscaches listessai nbreu essais aide listeaide listevariable
incr essais
    if {[lindex $listemotscaches $i] == [$t.ent$i get]} {
    incr nbreu
    if {$listessai($i)== 0} {tux_reussi} else {tux_continue_bien}
    catch {destroy .menu.lab}
    label .menu.lab -text [format [mc {%1$s signe(s) trouve(s) sur %2$s.}] $nbreu [llength $listemotscaches]]
    pack .menu.lab

    bind $t.ent$i <Return> {}
    $t.ent$i configure -state disabled -fg blue
    testefin $nbreu $essais
    } else {
  if {$listessai($i) >= 1} {tux_echoue2} else {tux_echoue1}
  incr listessai($i) 
    $t.ent$i delete 0 end

    }
affichecouleur $i $t
}

proc testefin {nbreu essais} {
global sysFont listemotscaches user categorie
    if {$nbreu >= [llength $listemotscaches]} {
    catch {destroy .menu.lab}
    set str0 [mc {Exercice termine en }]
    set str2 [format [mc {%1$s essai(s) pour %2$s signe(s).}] $essais $nbreu]
    set str1 [mc {Exercice termine en }]
    #enregistreval $str1 $categorie $str2 $user
set score [expr ($nbreu*100)/([llength $listemotscaches] +($essais- $nbreu))]
if {$score <50} {tux_triste $score}
if {$score >=50 && $score <75 } {tux_moyen $score}
if {$score >=75} {tux_content $score}

    label .menu.lab -text $str0$str2
    pack .menu.lab
    bell
        }
}


proc affichecouleur {ind t} {
global sysFont listessai
switch $listessai($ind) {
    0 { $t.ent$ind configure -bg yellow}
    1 { $t.ent$ind configure -bg green}
    default { $t.ent$ind configure -bg red}
    }
}

if {$startdirect == 0 } {
main .text
}

proc abandon {t} {
global sysFont plist listemotscaches listessai texte nbmotscaches auto longchamp longmot nbreu essais
global sysFont longchamp longmot listemotscaches essais motfaux
set score [expr ($nbreu*100)/([llength $listemotscaches] +($essais- $nbreu))]
if {$score <50} {tux_triste $score}
if {$score >=50 && $score <75 } {tux_moyen $score}
if {$score >=75} {tux_content $score}

set cur 0.0
$t configure -state normal
catch {destroy .menu.b2}
catch {destroy .menu.lab}

set listtag [$t tag names]
    foreach tg $listtag {
catch {
       set ind [lsearch $tg "ent*"]
       if {$ind != -1} {

          set nom $tg
          set num [string map {ent ""} $nom]
          set motfaux [$t get $nom.first $nom.last]
          #set cur [$t search $motfaux $cur end]
          set cur [$t index $nom.first]

          $t delete $nom.first $nom.last
          $t insert $cur [lindex $listemotscaches $num]
          $t tag add violet $cur "$cur + [string length [lindex $listemotscaches $num]] char"


        $t tag bind text <ButtonRelease-1> {}

       }
}
}
$t tag configure violet -background pink

$t configure -state disabled

}

proc fin {} {
global sysFont categorie user essais listemotscaches nbreu listexo iwish filuser repertoire lecture_mot_cache
variable repertconf

    set score [expr ($nbreu*100)/([llength $listemotscaches] +($essais- $nbreu))]
    set str0 [mc {Exercice termine en }]
    set str2 [format [mc {%1$s essai(s) pour %2$s signe(s) sur %3$s.}] $essais $nbreu [llength $listemotscaches]]

    set str1 [mc {Exercice Ponctuation 2}]

switch $lecture_mot_cache {
1 { set lectmotcacheconf [mc {Le texte peut �tre entendu.}]}
0 { set lectmotcacheconf [mc {Le texte ne peut pas �tre entendu.}]}
}

set exoconf [mc {Parametres :}]
set exoconf "$exoconf  Son : $lectmotcacheconf"

    enregistreval $str1\040[lindex [lindex $listexo 12] 1] \173$categorie\175 $str2 $score $repertconf 12 $user $exoconf $repertoire
exec $iwish aller.tcl $filuser &
exit

}

proc boucle {} {
global  categorie startdirect essais nbreu listexo auto listemotscaches  user
    set str0 [mc {Exercice termine en }]
    set str2 [format [mc {%1$s essai(s) pour %2$s signe(s) sur %3$s.}] $essais $nbreu [llength $listemotscaches]]
    set str1 [mc {Exercice Ponctuation 2}]
    enregistreval $str1 \173$categorie\175 $str2 $user
set essais 0
set nbreu 0
set listexo ""
set listemotscaches ""
set nbmotscaches 0
set categorie "Au choix"
.text configure -state normal
set auto [charge .text $categorie]
.text configure -state disabled
focus .text
.menu.b1 configure -text [mc {Commencer}] -command "main .text"
catch {destroy .menu.suiv}
catch {destroy .menu.b2}

if {$startdirect == 0 } {
main .text
}
}



