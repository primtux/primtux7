Le paon (2).
Il ne voit rien venir et personne ne r�pond. Les volailles habitu�es ne l�vent m�me point la t�te. Elles sont lasses de l'admirer. Il redescend dans la cour, si s�r d'�tre beau qu'il est incapable de rancune. Son mariage sera pour demain.
Et, ne sachant que faire du reste de la journ�e, il se dirige vers le perron. Il gravit les marches, comme des marches de temple, d'un pas officiel. Il rel�ve sa robe � queue toute lourde des yeux qui n'ont pu se d�tacher d'elle. Il r�p�te encore une fois la c�r�monie.
Jules Renard.