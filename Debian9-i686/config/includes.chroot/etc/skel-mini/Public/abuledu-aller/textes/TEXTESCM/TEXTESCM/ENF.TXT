L'enfant invisible.(1) 
Un soir sombre et pluvieux, la famille Moumine �tait r�unie autour de la table sous la v�randa pour �plucher des champignons. Elle avait recouvert la table de papier journal et mis la lampe � p�trole au milieu, mais les coins de la pi�ce �taient plong�s dans le noir.
� Mu a encore cueilli des russules poivr�es, dit Papa Moumine. L'ann�e derni�re, c'�tait des tue-mouches. 
- Esp�rons que la prochaine fois elle cueillera des girolles, dit Maman Moumine. Ou du moins des vesses-de-loup.
- L'espoir fait vivre !� dit la petite Mu goguenarde.
Ils continu�rent leur travail en silence. Soudain on frappa l�g�rement sur la porte vitr�e et, sans attendre de r�ponse, Touticki entra en secouant pour faire tomber les gouttes de pluie de son cir�. Elle avait laiss� laporte ouverte sur la nuit et appela :
� Allons, viens !
-Qui est l� ? demanda Moumine le Troll.
-Nini, dit Touticki. Elle s'appelle Nini. �
Ils attendirent. Personne ne venait. 
