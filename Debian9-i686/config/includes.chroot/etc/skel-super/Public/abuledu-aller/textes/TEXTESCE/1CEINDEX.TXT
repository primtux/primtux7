CE001.txt
La petite locomotive rouge roule sous la table. Elle va rouler sous les chaises. Quand elle veut bien rester sur les rails, le petit gar�on est heureux.
CE002.txt
Un petit gar�on entre dans le magasin. Une dame ach�te du lait, une casserole et des pommes de terre. Le marchand la sert avec un sourire.
CE003.txt
Le vent malin emporte un journal, le chapeau de la petite fille, le parapluie d'un monsieur et le ballon d'un enfant qui monte tr�s haut et tr�s vite dans le ciel. 
CE004.txt
Une vieille dame dit : " Je vais faire une bonne soupe. " 
Dans la marmite, elle verse de l'eau. Quand elle bout, elle ajoute un chou vert, une grosse pomme de terre, quatre carottes et du sel. Quel r�gal !
CE005.txt
Dans la haie, la vieille bouilloire est toute triste. Elle pleure car elle a un gros trou. Le lapin s'installe dans la bouilloire, avec le l�zard et la souris. La vieille bouilloire est heureuse avec tous ses amis.
CE006.txt
Les ciseaux veulent du travail. Ils taillent l'herbe du jardin, le poil du chien et la queue du cheval. Une vieille dame rentre � la maison et les pend � un clou bien brillant.
CE007.txt
Le chien va jouer dans le jardin. Il prend la jolie poup�e par le bras et la fait tomber dans l'eau du bassin. On doit r�parer la pauvre poup�e.
CE008.txt
Le lion est le roi des animaux. Pour son anniversaire, il re�oit beaucoup de cadeaux. C'est la f�te. Tous les animaux sont joyeux ; ils chantent une chanson.
CE009.txt
Eric a six ans. Le facteur lui apporte dix cartes d'anniversaire. Eric re�oit un livre pour lire et un beau ballon pour jouer. Son oncle lui donne des pi�ces neuves et un avion � h�lice. Eric les remercie tous.
CE010.txt
Une petite fille, qui adore les bijoux, pleure car elle a perdu sa bague. M�me ses trois petits oiseaux et ses deux petits lapins ne peuvent la consoler. Une f�e la lui retrouve. La petite fille est contente. 
CE011.txt
Sur le chemin de l'�cole, Paul a trouv� une petite cl�. Elle ne peut pas ouvrir la porte de la voiture ou la bo�te � lettres. Grand-m�re ouvre son sac et peut remonter sa montre avec la petite cl�.
CE012.txt
Jour et nuit, la pendule sur le mur dit � chacun ce qu'il doit faire. Papa la regarde et se l�ve le premier. Maman demande l'heure et pr�pare le caf�. 
" C'est l'heure d'aller � l'�cole ", dit Herv�.
CE013.txt
Oubangui est une grande et belle rivi�re. Elle coule en Afrique dans un  pays tr�s chaud. Un petit gar�on noir a faim. Il mange un poisson. Il a soif et boit l'eau claire et fra�che de la cascade. Le petit gar�on est tr�s fort. Il plonge dans les vagues avec courage. Fatigu�, il monte dans un bateau. Oubangui berce le bateau tout doucement. Le petit gar�on s'endort, berc� par la rivi�re.
CE014.txt
Wanda est une gentille sorci�re. Elle a un chat noir. Tous les deux, ils ont fabriqu� une fus�e. Leur fus�e peut aller plus vite qu'un avion. Wanda a sauv� un petit gar�on poursuivi par un crocodile. Elle a sauv� un deuxi�me gar�on au milieu d'une for�t en feu. Elle sauve aussi un troisi�me gar�on perdu dans la montagne.
CE015.txt
Le ciel est bleu. Tout en haut de la montagne, il fait tr�s froid. Tu emporteras un manteau de laine, un bonnet et une �charpe. Nous marcherons lentement. Au sommet, nous ferons un drapeau avec un b�ton et un mouchoir.
CE016.txt
Pour savoir comment il doit travailler et comment il doit manger, le b�b� �l�phant �coute bien les le�ons de sa maman. Il remue les oreilles, baisse sa trompe et incline la t�te. Il est tr�s intelligent.
CE017.txt
Pierre, son fr�re et sa soeur veulent fabriquer une luge. Ils prennent du bois dans le hangar. Le menuisier coupe les c�t�s des planches. Le forgeron fait des trous et visse les patins. Leur papa visse le si�ge et deux anneaux pour passer une corde. Ensuite, avec leur luge, les enfants courent vers une prairie en pente. Ils glissent vite, toujours plus vite.
CE018.txt
Une petite souris croit que la lune est tomb�e dans la mare. Elle appelle au secours au milieu de la nuit. Les soldats du roi galopent vers la mare avec des filets, des crochets, des cha�nes. Soudain, le roi l�ve la t�te. Les soldats, le fermier, le chien, le cheval et la souris regardent vers le ciel. Ils voient la lune ronde qui brille. Tout le monde se met � rire.



CE019.txt
Trois ch�vres veulent aller dans une �le brouter l'herbe verte. Mais elles ne peuvent pas nager si loin. Elles font tomber dans l'eau un fermier, son fils et sa fille pour qu'ils nagent jusqu'� l'�le. Ensuite, elles vont chercher la femme du fermier. Elle doit conduire les ch�vres dans l'�le pour retrouver son mari et ses enfants. Les trois ch�vres rus�es se r�galent alors d'herbe verte et tendre.
CE020.txt
Notre chien garde la maison tout seul. Soudain, apr�s notre d�part, il voit le tapis et un rideau en feu. Il saute sur le bord de la fen�tre de la cuisine. Un gar�on boucher passe dans la rue. Il entend le chien qui aboie tr�s fort et t�l�phone aux pompiers. Notre chien est un h�ros !
CE021.txt
Souvent, deux enfants vont jouer chez leur oncle. Un jour, ils ont trouv� une feuille de papier toute jaune, au pied d'un vieux mur. Ils lisent : " Tu vas suivre la piste et tu trouveras un tr�sor dans le grenier. Il est cach� dans les restes d'un navire. "
CE022.txt
" Je veux me battre ", dit un jeune lion.  " Je veux donner des coups de bec ", dit un jeune aigle. Un grand hippopotame gris sort de l'eau. Il se roule dans la boue lentement, calmement. " Pour �tre heureux, il faut du calme ", dit l'hippopotame. Il pousse un soupir et s'endort. " Il a peut-�tre bien raison ", dit l'�l�phant. Les animaux partent tout doucement.
CE023.txt
Dans la nuit de l'hiver, galope un grand homme blanc. C'est un bonhomme de neige avec une pipe en bois. Un grand bonhomme de neige poursuivi par le froid. Il arrive au village. Voyant de la lumi�re, le voil� rassur�. Dans une petite maison, il entre sans frapper et pour se r�chauffer s'assoit sur le po�le rouge. D'un coup, il dispara�t, ne laissant que sa pipe au milieu d'une flaque.
CE024.txt
Les trois chats noirs ne sont pas sages. Ils vont sauter sur la plus haute branche du pommier pour entrer dans la maison. Grand-m�re a l'air de ne rien voir. Elle se cache dans une armoire. Quand les chats vont descendre l'escalier, elle leur crie : " Dehors ! ".
CE025.txt
Une demoiselle se regarde devant un miroir. Elle a des rubans de toutes les couleurs autour de la taille et un bouquet blanc � la main. Elle a aussi un long voile et une longue robe. Elle va se marier tout � l'heure.
CE026.txt
Un petit gar�on veut rendre la libert� au tigre qui tourne en rond dans sa cage, au lion et aux oiseaux. Il va trouver le directeur du cirque et un gardien. Il ne peut pas acheter les animaux car il n'a pas beaucoup d'argent.
CE027.txt
Le vent fait tourner les ailes du moulin, pousse les voiles du bateau et s�che la terre ou le linge. Quand il souffle en temp�te, il peut casser des arbres et emporter des toits de maison.
CE028.txt
Marc a un v�ritable petit zoo chez lui. Il donne des petits morceaux de viande pain � ses chiens, offre de la salade verte � ses escargots et des carottes � ses lapins.  Plus tard, Marc remet des animaux en libert� : les lapins sur la colline et les escargots dans le jardin.
CE029.txt
Le jeune chameau n'aime pas marcher au soleil dans le sable chaud et sec. Il ne veut pas dormir sur le sol dur. Un matin, tr�s t�t, alors que tout le monde dort, il quitte sa m�re. Il esp�re ainsi arriver dans un autre d�sert.
CE030.txt
La ch�vre de Monsieur Seguin (1).
Ah ! Gringoire, qu'elle �tait jolie la petite ch�vre de M. Seguin ! Qu'elle �tait jolie avec ses yeux doux, sa barbiche de sous-officier, ses sabots noirs et luisants, ses cornes z�br�es et ses longs poils blancs qui lui faisaient une houppelande ! C'�tait presque aussi charmant que le cabri d'Esm�ralda, tu te rappelles, Gringoire? - Et puis, docile, caressante, se laissant traire sans bouger, sans mettre son pied dans l'�cuelle. Un amour de petite ch�vre... 
Alphonse Daudet.
CE031.txt
La ch�vre de Monsieur Seguin (2).
" - Ecoutez, monsieur Seguin, je me languis chez vous, laissez-moi aller dans la montagne. 
- Ah ! mon Dieu ! ... Elle aussi ! ", cria M. Seguin stup�fait, et du coup il laissa tomber son �cuelle ; puis, s'asseyant dans l'herbe � c�t� de sa ch�vre : 
" - Comment, Blanquette, tu veux me quitter ! "
Alphonse Daudet.
CE032.txt
La ch�vre de Monsieur Seguin (3).
Et Blanquette r�pondit : 
" - Oui, monsieur Seguin. 
- Est-ce que l'herbe te manque ici ? 
- Oh ! Non ! monsieur Seguin. 
- Tu es peut-�tre attach�e de trop court, veux-tu que j'allonge la corde ? 
- Ce n'est pas la peine, monsieur Seguin.
- Alors, qu'est-ce qu'il te faut ? Qu'est-ce que tu veux ? 
Alphonse Daudet.


CE033.txt
La ch�vre de Monsieur Seguin (4). 
- Je veux aller dans la montagne, monsieur Seguin.
- Mais, malheureuse, tu ne sais pas qu'il y a le loup dans la montagne... Que feras-tu quand il viendra ?... 
- Je lui donnerai des coups de corne, monsieur Seguin. 
- Le loup se moque bien de tes cornes. Il m'a mang� des biques autrement encorn�es que toi... Tu sais bien, la pauvre vieille Renaude qui �tait ici l'an dernier ? Une ma�tresse ch�vre, forte et m�chante comme un bouc. Elle s'est battue avec le loup toute la nuit... puis, le matin, le loup l'a mang�e.
Alphonse Daudet.
CE034.txt
La ch�vre de Monsieur Seguin (5).
- P�ca�re ! Pauvre Renaude !... �a ne fait rien, monsieur Seguin, laissez-moi aller dans la montagne. 
- Bont� divine !... dit M. Seguin ; mais qu'est-ce qu'on leur fait donc � mes ch�vres ? Encore une que le loup va me manger... Eh bien, non... je te sauverai malgr� toi, coquine ! Et de peur que tu ne rompes ta corde, je vais t'enfermer dans l'�table, et tu y resteras toujours. "
Alphonse Daudet.
CE035.txt
Voracit� (1).
Mon fr�re Paul �tait un petit bonhomme de trois ans, la peau blanche, les joues rondes, avec de grands yeux d'un bleu tr�s clair, et les boucles dor�es de notre grand-p�re inconnu. Il �tait pensif, ne pleurait jamais, et jouait tout seul, sous une table, avec un bonbon ou un bigoudi.
Marcel Pagnol.
CE036.txt
Voracit� (2).
Mais sa voracit� �tait surprenante : de temps � autre, il y avait un drame �clair : on le voyait tout � coup s'avancer, titubant, les bras �cart�s, la figure violette. Il �tait en train de mourir suffoqu�.
Marcel Pagnol.
CE037.txt
Voracit� (3).
Ma m�re affol�e frappait dans son dos, enfon�ait un doigt dans sa gorge, ou le secouait en le tenant par les talons, comme fit jadis la m�re d'Achille. Alors, dans un r�le affreux, il expulsait une grosse olive noire, un noyau de p�che, ou une longue lani�re de lard. Apr�s quoi, il reprenait ses jeux solitaires, accroupi comme un gros crapaud. 
Marcel Pagnol.
CE038.txt
Les b�b�s m�sanges (1).
Quelques jours plus tard, le premier b�b� m�sange sort de son oeuf. Minuscule et si fragile, il ne voit m�me pas clair. Et il est tout nu, sans plumes du tout. Heureusement, le nid est chaud et confortable. Brrr... Il doit faire bon alors se cacher bien au chaud sous les ailes de sa maman. 
CE039.txt
Les b�b�s m�sanges (2).
Quels piaillements ! Maintenant, leurs plumes commencent � pousser. Ils vivent le bec ouvert, les goulus ! Mais comment font-ils pour manger ? Papa et maman vont et viennent, le bec plein de provisions.
CE040.txt
Les b�b�s m�sanges (3).
Et hop ! Ils se penchent vers un bec affam� et y d�posent la nourriture : c'est cela, donner la becqu�e. Je vois une petite langue mais ils n'ont pas de dents ! Ils n'en auront jamais ; les oiseaux n'en ont pas. Leur bec les remplace, mais celui des petits, entour� d'un gros bourrelet jaune, n'est pas encore assez solide.
CE041.txt
L'�cole d'autrefois (1).
Tous les matins, au petit jour, le p�re m'�veillait. Les fr�res et les s�urs dormaient encore ; je m'habillais sans faire de bruit et je sortais avec mon petit sac, les pieds dans mes sabots et ma b�che sous le bras.
CE042.txt
L'�cole d'autrefois (2).
Il faisait froid � l'entr�e de l'hiver ; je fermais bien la porte et je repartais, soufflant dans mes doigts. J'entrais dans la classe encore vide. Je posais ma b�che et mes sabots � c�t� du po�le pour les s�cher. Chacun devait balayer � son tour ; mais je commen�ais en attendant les autres.
CE043.txt
Piqu�e par une vip�re (1).
Quelques jours apr�s la fin de l'�cole, je suis all�e avec ma grand-m�re chercher des oeufs chez ma tante. Pour y arriver, je traverse un champ en friche et je saute sur une vip�re qui se dorait au soleil. Nous arrivons. Mon pied enfle. Ma grand-m�re veut repartir mais ma tante l'en emp�che.


CE044.txt
Piqu�e par une vip�re (2).
Elle me dit : " Va chercher les vaches et cela passera ". Mais pendant ce temps, mon pied enfle toujours. Revenues � la maison, nous appelons le docteur. Il dit : " Elle a bel et bien �t� mordue ! " Quatre ou cinq jours apr�s, mon pied me fait toujours mal et je pense que les vacances commencent vraiment bien !
CE045.txt
Le verre (1).
Le verre est fragile et il est plus lourd que la mati�re plastique. Mais il est plus dur et ne se raye pas facilement. On sait fabriquer le verre depuis tr�s longtemps. On m�lange du sable avec de la potasse. On chauffe tr�s fort pour que tout devienne liquide. Lorsque tout est refroidi, on a le verre transparent.
CE046.txt
Le verre (2).
On donne au verre la forme que l'on veut en le travaillant pendant qu'il est encore chaud. On le fait couler sur des grandes plaques pour faire les vitres. On le souffle dans des moules pour faire des bouteilles. On peut aussi le tailler lorsqu'il est refroidi pour faire de jolis objets.
CE047.txt
Le nid des oiseaux (1).
Le nid, c'est la maison des oiseaux. En parlant de nid, la premi�re chose � dire, c'est : " Ne touchez pas aux nids ". Certains oiseaux l'abandonnent d�s qu'ils s'aper�oivent que nous l'avons vu. Si parfois la m�re reste, c'est que ses petits sont sur le point d'�clore.
CE048.txt
Le nid des oiseaux (2).
Tous les oiseaux ne cachent pas leur nid aux m�mes endroits. Certains oiseaux, comme la pie, le construisent au sommet des arbres. D'autres, comme le roitelet ou le pinson, le cachent dans les branches. Le merle, la tourterelle font leurs nids au plus profond des buissons �pineux. Certains utilisent des trous dans un tronc d'arbre, dans un mur ou dans un rocher ; d'autres, enfin, les construisent � m�me le sol, dans les hautes herbes.
CE049.txt
Job, le p�cheur (1).
C'est un vieux p�cheur de plus de soixante-dix ans, un peu intimid� de se trouver devant tous les enfants. Mais il r�pond si gentiment � toutes nos questions ! " Je n'ai pas toujours �t� p�cheur ! Autrefois, je naviguais sur un grand voilier. Je suis parti le lendemain de mon certificat d'�tudes comme mousse sur un bateau.
CE050.txt
Job, le p�cheur (2).
Mon grand-p�re aussi �tait marin. Un jour, en cours de route, la patron du bateau mourut et mon grand-p�re ramena le bateau en France depuis la mer d'Islande. Et pourtant, il ne savait ni lire ni �crire. On n'allait pas beaucoup � l'�cole � cette �poque l� ! Et la vie �tait dure sur les bateaux ! Nous dormions sur la paille ; nous ne mangions presque pas de viande, mais beaucoup de poissons et quelques pommes de terre. "
CE051.txt
A la f�te (1).
Comme nous nous sommes amus�s cet apr�s-midi ! Maman nous avait donn� 1,50 F � chacun, et nous avons �t� � la f�te de la place Blanche, o� j'avais tant envie d'aller. J'�tais tellement contente qu'en essuyant la vaisselle, j'ai cass� la soucoupe de la tasse bleue, mais tant pis !
CE052.txt
A la f�te (2).
Maman nous avait recommand� de tenir Riquet par la main pour qu'il ne se perde pas, mais elle avait oubli� de dire si ce devait �tre Estelle ou moi ; alors on s'est disput�es et, pour finir, on lui a pris chacune une main ; il �tait furieux, mais comme dit Estelle, nous sommes les a�n�es et il faut bien qu'il nous ob�isse. 
CE053.txt
A la f�te (3).
Et puis, tout de suite, on a recommenc� � se disputer parce que chacun voulait monter sur quelque chose de diff�rent. On criait : Riquet : " Sur les avions ! "  Estelle : " Sur les chevaux qui montent et qui descendent ! " Moi : " Sur les autos tamponneuses ! " Moi, j'aimais plut�t mieux les balan�oires, les rouges surtout.
CE054.txt
A la f�te (4).
Finalement, on a choisi les chevaux : 50 centimes le tour, ce n'�tait pas cher, d'autant plus qu'ils �taient tr�s beaux et qu'ils tournaient � une vitesse... en haut, en bas, en haut, en bas... Au d�but, c'est amusant, mais apr�s, on se sent dr�le et, quand �a s'est arr�t�, j'avais mal au c�ur.
CE055.txt
Un nichoir pour la m�sange (1).
- Est-ce que la m�sange reviendra faire son nid dans la bo�te � lettres, l'ann�e prochaine ? demande Vincent.
- J'esp�re que non, r�pond papa, ce n'est pas tr�s confortable.
- Dans les bois, o� les m�sanges font-elles leurs nids ?
- Habituellement, elles trouvent tr�s bien un endroit : un trou, un tronc d'arbre creux, une fente dans un mur... 

CE056.txt
Un nichoir pour la m�sange (2).
Mais elles acceptent aussi les logements qu'on leur propose. Nous allons construire un nichoir.
Et voil� pourquoi Vincent est transform� en menuisier, ce matin. Un nichoir, c'est une bo�te en bois, avec un petit trou rond qui sert de porte et un c�t� qui glisse pour voir si le nichoir est habit� et le nettoyer.
CE057.txt
Que mange-t-on � Tahiti ? (1).
Des noix de coco, bien s�r ! Le cocotier, qui se balance sur toutes les plages, vit cent ans et, d�s la septi�me ann�e, produit sans cesse des noix. Le lait de noix de coco est tr�s employ�, soit pur, en boisson, soit dans la cuisson des plats.
CE058.txt
Que mange-t-on � Tahiti ? (2).
L'amande du coco (coprah), s�ch�e, est moulue pour en extraire une huile dont on fait un beurre et du savon. Le coprah est, avec la vanille, une des principales ressources de Tahiti. Le poisson est � la base de la nourriture tahitienne. La grande occupation est donc la p�che et celle-ci se fait souvent en pirogue � balancier.
CE059.txt
La chasse du hibou (1).
Je vais vous expliquer comment chasse le hibou. Le hibou ne se met vraiment � vivre que lorsque la nuit tombe. Parlons d'abord de son cri. Quand un hibou pousse son hululement, les autres animaux s'effraient. S'ils font alors un mouvement, le hibou l'entend aussit�t gr�ce � son oreille extr�mement sensible.
CE060.txt
La chasse du hibou (2).
Une fois qu'il a r�ussi � faire peur � un rat ou � une souris et qu'il l'a entendu bouger, le hibou s'en approche et l'aper�oit tout de suite. Le hibou voit tr�s bien dans l'obscurit� car il peut ouvrir tr�s grand ses pupilles, ce qui lui permet d'utiliser le peu de lumi�re qui reste toujours sur terre, m�me au milieu de la nuit. Le hibou peut s'approcher de sa proie sans �tre entendu car ses plumes sont tr�s douces et tr�s fines. Il vole sans bruit et peut saisir sa victime sans qu'elle ait eu le temps de s'enfuir.
CE061.txt
L'histoire de Lisa (1).
Aujourd'hui, Lisa et J�r�me rentrent ensemble de l'�cole. Lisa a sept ans et J�r�me en a six.
- Je vais te raconter une histoire, dit Lisa.
- Voil�. Il �tait une fois une belle jeune fille. Elle habitait une chaumi�re dans la for�t.
CE062.txt
L'histoire de Lisa (2).
- Qu'est-ce que c'est, une chaumi�re ?, demande J�r�me.
- Tu sais bien, r�pond Lisa, c'est l'endroit o� habitent les gens dans les histoires. 
- C'est un appartement, alors ?
- Non, pas un appartement. Elle habitait une chaumi�re : c'est comme une petite maison.
CE063.txt
L'histoire de Lisa (3).
- Comme Madame Brunet, dit J�r�me. Elle habite aussi une petite maison. On va la d�molir et elle sera oblig�e d'habiter un appartement. Mais elle ne veut pas. 
- Si, elle veut bien, dit Lisa. Elle a dit que ce sera bien pour ses vieilles jambes. Elle aura un ascenseur et elle aura une salle de bains.
- Non, elle ne veut pas ! dit J�r�me. Elle a dit qu'elle ne veut pas habiter une pi�ce minuscule et passer son temps dans une baignoire.
CE064.txt
L'histoire de Lisa (4).
- Tu l'�coutes, mon histoire, oui ou non ? crie Lisa.
- Oui, dit J�r�me, vas-y.
- Voil�. Alors cette jeune fille habitait une chaumi�re dans la for�t.
- Qu'est-ce que c'est, la for�t ?
- Oh ! Zut ! Il y a des for�ts dans toutes les histoires. 
- Mais qu'est-ce que c'est ? 
- C'est  recouvert de feuilles, dit Lisa.
- Comme la voiture de Monsieur Clerc, au march� ?, dit J�r�me...
CE065.txt
Insectes ennemis (1).
Il existe plus de cinq cent mille types d'insectes. Il ne faut donc pas s'�tonner que certains nous g�nent ! Ils ont besoin de manger et d�vorent les aliments que nous destinons � notre nourriture, les graines que nous cultivons dans nos fermes, les l�gumes de nos jardins, les fruits de nos vergers. 
CE066.txt
Insectes ennemis (2).
Chaque fois que nous tombons sur une pomme, une carotte ou un �pi de ma�s v�reux, nous pouvons nous dire : " Tiens, nos voisins � six pattes sont arriv�s avant nous ! " En effet, les " vers " que nous trouvons dans les fruits, les l�gumes, ne sont qu'une �tape dans la vie d'un insecte. On les appelle des chenilles, ou des larves. Certains insectes viennent donc jusque sur nos tables essayer de prendre les aliments dans nos assiettes.
CE067.txt
Le brochet (1).
Le brochet est un poisson d'eau douce qui vit dans les rivi�res, les torrents, les lacs et les �tangs. Il peut mesurer un m�tre de long et peser vingt-cinq kilos. Il nage vite et fonce sur sa proie � pr�s de quarante kilom�tres � l'heure. 
CE068.txt
Le brochet (2).
Sa grande bouche est garnie de plus de sept cents dents dirig�es d'avant en arri�re ; elles sont tr�s coupantes et il est tellement f�roce qu'on le compare souvent � un petit requin. Toujours affam�, il chasse les poissons et les grenouilles ; il peut m�me s'attaquer � certains oiseaux et il lui arrive aussi de manger son semblable. 
CE069.txt
Le brochet (3).
Il joue un r�le utile puisqu'il d�barrasse les eaux de tous les poissons malades. Les p�cheurs sont toujours contents d'en attraper un car sa chair est tr�s bonne � manger. On le prend souvent en accrochant � l'hame�on un petit poisson vivant. Mais gare aux doigts lorsqu'on le d�croche !
CE070.txt
Dans le pr� (1).
Dans les champs et les pr�s, il y a de petits animaux qui n'aiment pas qu'on les d�range. Mais il y a des �tres qui s'agitent sans cesse ; on dirait qu'ils font du sport � longueur de journ�e. C'est ainsi que le dytique, sur la mare, fait du canotage ; il rame vigoureusement de ses six pattes.
CE071.txt
Dans le pr� (2).
Les moustiques et les moucherons dansent : ils valsent sans jamais s'arr�ter. L'alouette, elle, s'�lance comme un petit avion pour �tablir des records de hauteur. Il y a aussi les sauteurs, comme la grenouille et la sauterelle, qui s'entra�nent parmi le fouillis des herbes folles.
CE072.txt
La taupe (1).
La taupe a de tout petits yeux et elle y voit tr�s mal. Mais cela ne la g�ne gu�re puisqu'il fait noir sous terre. Par contre, elle entend tr�s bien et son odorat est tr�s d�velopp�, ce qui lui permet de chasser les vers et les insectes.
CE073.txt
La taupe (2).
Elle en d�vore de grandes quantit�s car elle ne peut rester sans manger plus de douze heures, ce qui la rend tr�s utile pour l'agriculture. Pourtant, elle n'est pas tr�s aim�e, car elle creuse de longues galeries dans la terre et coupe quelques racines au passage.
CE074.txt
La taupe (3).
Elle avance sous terre � l'aide de ses pattes en forme de pelle et de ses griffes tr�s dures. Et les " taupini�res " qui apparaissent alors au milieu des pelouses font le d�sespoir des jardiniers.
CE075.txt
Les pieuvres g�antes (1).
Pendant des si�cles, les marins ont racont� des histoires de pieuvres g�antes qui enveloppaient les bateaux avec leurs tentacules et les tiraient au fond de l'Oc�an. Et beaucoup de gens les croyaient et vivaient dans la crainte de voir surgir des monstres dont les bras d'une dizaine de m�tres �taient remplis de ventouses aussi larges qu'une assiette.
CE076.txt
Les pieuvres g�antes (2).
Mais des savants ont longtemps affirm� que de tels animaux ne pouvaient pas exister. Tout cela n'�tait que l�gende. Rien n'�tait moins s�rieux que l'existence des pieuvres gigantesques capables de saisir avec leurs bras non seulement des hommes mais encore
des cachalots ! Et pourtant, il fallut bien se rendre � l'�vidence...
CE077.txt
Les pieuvres g�antes (3).
On a retrouv� sur les c�tes de Terre-Neuve un calmar gigantesque de 17 m�tres de long et dont le diam�tre des yeux mesure 40 centim�tres. Quelques ann�es plus tard, on en d�couvre un qui mesure plus de 17 m�tres. On pense m�me qu'il en existe qui p�sent 3 tonnes et mesurent plus de 20 m�tres. On sait qu'ils vivent dans les grandes profondeurs. On en a pourtant vu s'attaquer � des navires.
CE078.txt
La l�gende du pont Valentr� de Cahors (1).
Jamais le pont sur le Lot n'aurait �t� achev� si le Diable ne s'en �tait m�l�. Le pauvre ma�on d�sesp�rait d'y parvenir et il avait appel� le Diable � son secours. En �change, il promet de lui abandonner son �me.
CE079.txt
La l�gende du pont Valentr� de Cahors (2).
Aussit�t, les travaux avanc�rent comme par miracle, et bient�t il ne resta plus qu'une pierre � poser, la pierre d'angle de la tour du milieu, tout en haut. Le ma�on voyait venir le moment o� il lui faudrait payer.




CE080.txt
La l�gende du pont Valentr� de Cahors (3).
Alors, il demanda un dernier service au d�mon : il lui demanda d'apporter, pour pr�parer le dernier ciment, l'eau d'une fontaine voisine mais en utilisant un tamis. Le Diable avait promis d'ex�cuter tous les ordres qui lui seraient donn�s. Il prit le tamis, se rendit � la fontaine.
CE081.txt
La l�gende du pont Valentr� de Cahors (4).
Il puisa l'eau demand�e, mais aussi vite qu'il cour�t, il ne parvint jamais � en porter une seule goutte jusqu'au chantier. Le ma�on avait gagn� et le Diable disparut sans emporter l'�me promise. Mais chaque fois que le ma�on essayait de sceller la pierre qui manquait, chaque fois, pendant la nuit, elle disparaissait...
CE082.txt
Les colporteurs (1).
Autrefois, du temps o� les grands-parents de nos grands-parents �taient encore des enfants, les colporteurs circulaient dans les campagnes et allaient de maison en maison. Ils portaient une grande bo�te qui contenait de tr�s nombreux petits objets qu'il �tait difficile de fabriquer � la maison : aiguilles, boutons, fil, lacets, etc.
CE083.txt
Les colporteurs (2).
C'est qu'� l'�poque, on ne se d�pla�ait pas comme �a ! Il n'y avait ni bicyclette, ni voiture ; il fallait �tre riche pour atteler un cheval � une charrette. Les hommes allaient bien, de temps en temps, � la foire, mais, pour toutes les petites choses qu'il faut dans une maison, le colporteur �tait le bienvenu. On l'aimait surtout parce qu'il apportait des nouvelles et qu'il parlait de pays qu'on ne connaissait pas.
CE084.txt
A l'int�rieur de la ruche (1).
L'int�rieur de la ruche est compos� d'un tr�s grand nombre de petites cavit�s dispos�es les unes � c�t� des autres. Ces petites cellules ressemblent aux pi�ces d'une maison. Le miel est rang� dans certaines cellules et le pollen dans d'autres. La reine d�pose ses oeufs dans les cellules vides.
CE085.txt
A l'int�rieur de la ruche (2).
Elle pond parfois plus de mille oeufs en une journ�e. De petites larves blanches qui doivent toutes �tre nourries, en sortent. Les jeunes ouvri�res nourrissent la reine et prennent soin des larves. Lorsque les larves ont assez mang�, les ouvri�res les enferment en recouvrant chaque cellule avec de la cire.
CE086.txt
A l'int�rieur de la ruche (3).
A l'int�rieur, la larve se transforme lentement en abeille. Lorsque la transformation est achev�e, la jeune abeille troue les couches de cire en les mordillant. Elle est � nouveau nourrie par les ouvri�res jusqu'� ce qu'elle soit assez forte pour voler au-dehors.
CE087.txt
Butinette, l'abeille (1).
Butinette, la petite abeille, loge dans le tronc du vieux tilleul. Elle va faire sa premi�re promenade dans le soleil printanier : le monde est bien grand ! Intimid�e, elle n'ose pas s'�loigner de la clairi�re verte et dor�e et suivre ses grandes s�urs dans leurs vols.
CE088.txt
Butinette, l'abeille (2).
Butinette trouve la vie merveilleuse. Des papillons volent autour des touffes de jonquilles, leurs ailes sont de mille couleurs. La coccinelle fait son march� et les sauterelles sautent plus haut que les coquelicots. Autour de l'essaim, tout �clate de joie. Mais voici le temps de faire le m�nage. Dans son nid, Butinette aide les ouvri�res � nettoyer les alv�oles. Courage !
CE089.txt
La grenouille et le crapaud (1).
Tout le monde a d�j� vu une grenouille, pos�e sur une feuille de n�nuphar, � l'aff�t d'insectes imprudents. On ne rencontre pas les grenouilles seulement au bord des mares, mais aussi dans les bois, les prairies et sur les talus. Vertes ou rousses, elles sont toujours tr�s farouches, bondissant � la moindre alerte.
CE090.txt
La grenouille et le crapaud (2).
En captivit�, la grenouille s'accommode d'un vivarium humide, avec un bassin rempli d'eau toujours claire, lui permettant de se baigner. La grenouille est un carnivore, vorace au point d'avaler toute proie vivante, insecte ou petit batracien.
CE091.txt
La grenouille et le crapaud (3).
A son allure pataude, on distingue facilement le crapaud de la grenouille. De moeurs terrestres, il recherche les lieux humides et frais ; c'est pourquoi on le rencontre parfois dans les caves, les celliers, les fosses et sous des pierres, pr�s des lavoirs.
CE092.txt
La grenouille et le crapaud (4).
Le crapaud est bien inoffensif ; toufefois, par pr�caution, n'oubliez pas de vous laver les mains apr�s l'avoir touch�. Le crapaud est aussi un animal tr�s utile qui a sa place dans le jardin autour de la maison. En libert�, il d�truit un grand nombre d'insectes.



CE093.txt
Carnivores et herbivores (1).
Les carnivores, c'est-�-dire ceux qui mangent d'autres animaux, sont de f�roces chasseurs qui doivent tuer pour vivre. Il leur faut de grandes dents puissantes pour d�chirer leurs proies. Mais ils ont �galement besoin de courir vite pour les attraper lorsqu'elles fuient.
CE094.txt
Carnivores et herbivores (2).
Les herbivores, au contraire, se nourrissent de v�g�taux. Ce sont le plus souvent des b�tes douces et paisibles et il arrive qu'un petit carnivore soit plus redoutable qu'un gros herbivore. Les animaux qui mangent des plantes ne sont pas n�cessairement rapides car les plantes sont enracin�es, et il n'est pas n�cessaire de courir apr�s elles.
CE095.txt
Carnivores et herbivores (3).
Toutefois, ces animaux peuvent �tre oblig�s de fuir rapidement pour �chapper aux carnivores. Aux herbivores, il faut des dents larges, �mouss�es, pour broyer des v�g�taux et, en outre, un estomac de grande capacit�, car leurs aliments sont peu nutritifs : ils doivent donc en absorber beaucoup.
CE096.txt
Carnivores et herbivores (4).
Certains herbivores ont un tube digestif tr�s compliqu� : ce sont des ruminants. Ce qu'ils avalent est d'abord emmagasin� dans une premi�re poche, puis, lorsqu'ils ont fini de trouver leur nourriture et qu'ils sont au calme, ils broient � nouveau ce qu'ils ont aval� avant de le faire passer dans l'estomac.
CE097.txt
Voici l'automne.
Le vent fait tournoyer les feuilles mortes. Les feuilles des h�tres se d�tachent, tombent lentement et se posent sur le sol. Le vent chante dans les branches des grands arbres. Les enfants, bien � l'abri dans un fourr�, �coutent les bruits de la for�t.
CE098.txt
En v�lo.
Il tombe de sa bicyclette, le choc est assez rude. Il se rel�ve aussit�t et regarde s'il y a quelque personne secourable � port�e de voix ou du regard. Il n'y a personne, les parents sont loin, ils n'ont rien vu. Alors le petit homme prend la d�cision de ne pas pleurer. Il remonte sur sa bicyclette et recommence � jouer.
CE099.txt
En v�lo.
Mon ami Pierre.
Pierre n'est pas grand pour son �ge. Il a les cheveux bruns et fris�s. Ses yeux vifs ont la couleur du ciel. Son nez est fin, ses joues sont ros�es. Il est toujours de bonne humeur. C'est mon meilleur ami. Nous sommes dans la m�me classe, nous allons � l'�cole ensemble.
CE100.txt
La f�te des rois.
Le jour de la f�te des rois, on partage une galette. Dans la galette, il y a une f�ve.
Celui qui trouve la f�ve est le roi. Il met sur sa t�te une couronne en carton dor�.
Il est le roi de la f�te, quand il boit, il faut dire tr�s fort : � le roi boit, vive le roi ! �. C'est Nicolas qui a trouv� la f�ve, elle avait la forme d'une �toile...