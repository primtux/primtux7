Le jardin sous la pluie.

La crois�e est ouverte ; il pleut 
Comme minutieusement, 
A petit bruit et peu � peu, 
Sur le jardin frais et dormant.

Feuille � feuille, la pluie �veille 
L'arbre poudreux qu'elle verdit ; 
Au mur, on dirait que la treille 
S'�tire d'un geste engourdi.

L'herbe fr�mit, le gravier ti�de 
Cr�pite et l'on croirait, l�-bas, 
Entendre sur le sable et l'herbe 
Comme d'imperceptibles pas.

Le jardin chuchote et tressaille.
Furtif et confidentiel ;
L'averse semble, maille � maille
Tisser la terre avec le ciel. 

Henri de R�gnier.
