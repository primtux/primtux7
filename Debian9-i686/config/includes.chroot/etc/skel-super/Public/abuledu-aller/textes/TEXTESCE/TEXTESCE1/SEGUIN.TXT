La ch�vre et le loup. 

La petite ch�vre de M. Seguin s'�tait sauv�e dans la montagne. Tout � coup le vent fra�chit. La montagne devint violette ; c'�tait le soir... � D�j� ! � dit la petite ch�vre ; elle s'arr�ta fort �tonn�e. 
En bas, les champs �taient noy�s de brume. Le clos de M. Seguin disparaissait dans le brouillard, et de la maisonnette on ne voyait plus que le toit avec un peu de fum�e. Elle �couta les clochettes d'un troupeau qu'on ramenait, et se sentit toute triste... Un gerfaut qui rentrait la fr�la de ses ailes en passant. Puis ce fut un hurlement dans la montagne : 
� Hou ! Hou ! � Elle pensa au loup ; de tout le jour la folle n'y avait pas pens�... Au m�me moment une trompe sonna dans la vall�e. C'�tait ce bon M. Seguin qui tentait un dernier effort. 
�Hou ! Hou !... � faisait le loup. 
� Reviens ! reviens!... � criait la trompe. 
Blanquette eut envie de revenir ; mais en se rappelant le pieu, la corde, la haie du clos, elle pensa que maintenant elle pouvait plus se faire � cette vie, et qu'il valait mieux rester. La trompe ne sonnait plus... 
La ch�vre entendit derri�re elle un bruit de feuilles. Elle se retourna et vit dans l'ombre deux oreilles courtes toutes droites, avec deux yeux qui reluisaient... C'�tait le loup.
