Le li�vre, mauvais payeur.
Une fois, un li�vre vint chez le Chien Cordonnier pour se faire faire une paire de souliers.
- Faites moi, lui dit-il, une paire de bottines ; j'ai terriblement froid aux pattes. 
Le chien se mit � l'ouvrage et confectionna une paire de bons et solides souliers que le li�vre enfila pour voir s'ils �taient � sa mesure... D�s qu'il sentit qu'ils lui allaient � merveille, hop ! il se sauva sans payer le cordonnier. Le chien, furieux, se lan�a � sa poursuite, mais en vain... Depuis ce temps, tous les chiens dans le monde cherchent le li�vre qui a emport� les souliers, mais je crains bien qu'ils ne le rattrapent jamais !
