Il y avait l� un pot d' oeillet tout en verdure, et il chantait � voix basse : -Le vent m'a caress�, le soleil m'a donn� une petite fleur, une petite fleur pour dimanche. 
Ensuite, le petit gar�on passa par une grande salle ; les murs �taient recouverts de cuir gaufr�, � fleurs et arabesques toutes dor�es, mais ternies par le temps. 
-La dorure passe, le cuir reste, marmottaient les murailles. 
Puis l'enfant fut conduit dans la chambre o� se tenait le vieux monsieur, qui l'accueillit avec un doux sourire, et lui dit : 
-Merci pour le soldat de plomb, mon petit ami; et merci encore de ce que tu es venu me voir. 
Et les hauts fauteuils en ch�ne, les grandes armoires et les autres meubles en bois des �les craquaient, et disaient : 
-knick, knack, ce qui pouvait bien vouloir dire : -Bien le bonjour !  Le soir, le petit gar�on rentra chez lui. Des semaines s'�coul�rent, et l' hiver arriva. Les fen�tres �taient gel�es, et l'enfant �tait oblig� de souffler longtemps sur les carreaux, pour y faire un rond par lequel il p�t apercevoir la vieille maison. Les sculptures de la porte, les tulipes, les trompettes, on les voyait � peine, tant la neige les recouvrait. La vieille maison paraissait encore plus tranquille et silencieuse que d'ordinaire; et, en effet, il n'y demeurait absolument plus personne: le vieux monsieur �tait mort, il s'�tait doucement �teint. 
(Extrait de: La vieille maison d'Andersen)

