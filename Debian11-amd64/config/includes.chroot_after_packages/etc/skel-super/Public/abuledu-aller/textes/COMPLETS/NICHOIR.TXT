Un nichoir pour la m�sange.
- Est-ce que la m�sange reviendra faire son nid dans la bo�te � lettres, l'ann�e prochaine ? demande Vincent.
- J'esp�re que non, r�pond papa, ce n'est pas tr�s confortable.
- Dans les bois, o� les m�sanges font-elles leurs nids ?
- Habituellement, elles trouvent tr�s bien un endroit : un trou, un tronc d'arbre creux, une fente dans un mur... Mais elles acceptent aussi les logements qu'on leur propose. Nous allons construire un nichoir.
Et voil� pourquoi Vincent est transform� en menuisier, ce matin. Un nichoir, c'est une bo�te en bois, avec un petit trou rond qui sert de porte et un c�t� qui glisse pour voir si le nichoir est habit� et le nettoyer.