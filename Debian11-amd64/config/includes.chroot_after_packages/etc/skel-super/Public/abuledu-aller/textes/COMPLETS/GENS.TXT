Les pauvres gens.

L'homme est en mer. Depuis l'enfance matelot, 
Il livre au hasard sombre une rude bataille. 
Pluie ou bourrasque, il faut qu'il sorte, il faut qu'il aille 
Car les petits enfants ont faim. Il part le soir, 
Quand l'eau profonde monte aux marches du musoir. 
Il gouverne � lui seul sa barque � quatre voiles. 

La femme est au logis, cousant les vieilles toiles, 
Remaillant les filets, pr�parant l'hame�on, 
Surveillant l'�tre o� bout la soupe de poisson 
Puis priant Dieu sit�t que les cinq enfants dorment. 

Lui, seul, battu des flots qui toujours se reforment, 
Il s'en va dans l'ab�me, et s'en va dans la nuit. 
Dur labeur ! tout est noir, tout est froid ; rien ne luit. 
Dans les brisants, parmi les lames en d�mence, 
L'endroit bon � la p�che, et, sur la mer immense, 
Le lieu mobile, obscur, capricieux, changeant, 
O� se pla�t le poisson aux nageoires d'argent, 
Ce n'est qu'un point; c'est grand deux fois comme la chambre. 
Or, la nuit, dans l'ond�e et la brume, en d�cembre, 
Pour rencontrer ce point sur le d�sert mouvant, 
Comme il faut calculer la mar�e et le vent ! 
Comme il faut combiner s�rement les manoeuvres ! 
Les flots le long du bord glissent, vertes couleuvres, 
Le gouffre roule et tord ses plis d�mesur�s
Et fait r�ler d'horreur les agr�s effar�s. 

Lui, songe � sa Jeannie, au sein des mers glac�es, 
Et Jeannie en pleurant l'appelle ; et leurs pens�es 
Se croisent dans la nuit, divins oiseaux du coeur. 

Victor Hugo.