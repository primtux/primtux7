R�volution dans le calendrier. 
-C'est une castastrophe... une castastrophe, Majest�. 
-Que se passe-t-il ? demande le Roi calendrier. 
- Les mois de l'ann�e... les mois de l'ann�e ont d�cid� de faire la r�volution, explique une vieille semaine en s'endormant. 
- La r�volution ! hurle le Roi. Mais que veulent-ils donc ? 
Un petit jour prend alors la parole. 
- Monsieur Janvier est toujours enrhum� et voudrait prendre la place de Monsieur Juin. 
Mademoiselle Mai en a assez de la couleur de ses robes et veut changer avec Madame Septembre. 
Quant � Monsieur Ao�t, il est malheureux : il ne voit jamais personne, car tout le monde part en vacances. Monsieur Novembre... 
- Cela suffit, cela suffit, l'interrompt le Roi Calendrier. Dites au 29 F�vrier de venir : 
lui qui est si malin saura trouver une solution. Tout le monde cherche partout le 29 f�vrier. Les semaines veulent demander aux week-end, mais la plupart d'entre eux sont partis se promener. Les jours questionnent leurs voisines les nuits. 
Le 29 f�vrier reste introuvable. 
C'est alors que le 28 f�vrier et le 1er mars s'approchent du Roi Calendrier. 
- Le 29 f�vrier est parti en Egypte, en Inde et m�me en Chine, d�clarent ensemble les deux jours. Comme on a besoin de lui une ann�e sur quatre, il se sent inutile et il est malheureux. 
