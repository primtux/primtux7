Voracit� (2).
Mais sa voracit� �tait surprenante : de temps � autre, il y avait un drame �clair : on le voyait tout � coup s'avancer, titubant, les bras �cart�s, la figure violette. Il �tait en train de mourir suffoqu�.
Marcel Pagnol.