PINOCCHIO.

Gepetto, le menuisier, a fabriqu� Pinocchio, un pantin. Un jour, Pinocchio r�chauffe ses pieds gel�s sur le po�le. A son r�veil, ils sont r�duits en cendres...
- Je veux des pieds ! je veux des pieds !
- Est-ce pour t'enfuir de la maison ? lui demanda Gepetto.
- Non,  je vous promets d'�tre sage d�sormais.
- Tous les enfants disent cela quand ils d�sirent quelque chose.
- Moi,  j'irai �  l'�cole, j'�tudierai et je serai un �l�ve parfait.
- Tous les enfants disent cela  quand ils d�sirent quelque chose, r�p�ta Gepetto.
- Moi, je ne suis pas comme les autres enfants, assura le pantin. Je suis meilleur qu'eux, je dis toujours la v�rit�.
Gepetto se laissa attendrir par ces belles paroles ; sans rien dire, il prit ses outils et deux morceaux de bois sec. Moins d'heure plus tard, les pieds �taient termin�s et fort bien faits. D�s qu'ils furent coll�s � ses jambes, le pantin sauta de la table et commmen�a � cabrioler dans toute la pi�ce.
- Pour vous remercier de votre gentillesse, annon�a-t-il, je veux aller � l'�cole tout de suite.
- Tu es un bon enfant, dit Gepetto en souriant.
