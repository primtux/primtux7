Voracit� (3).
Ma m�re affol�e frappait dans son dos, enfon�ait un doigt dans sa gorge, ou le secouait en le tenant par les talons, comme fit jadis la m�re d'Achille. Alors, dans un r�le affreux, il expulsait une grosse olive noire, un noyau de p�che, ou une longue lani�re de lard. Apr�s quoi, il reprenait ses jeux solitaires, accroupi comme un gros crapaud. 
Marcel Pagnol.