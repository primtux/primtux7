La vieille maison (10).
Il y avait l� un pot d'oeillet tout en verdure, et il chantait � voix basse : � Le vent m'a caress�, le soleil m'a donn� une petite fleur, une petite fleur pour dimanche. �
Ensuite, le petit gar�on passa par une grande salle ; les murs �taient recouverts de cuir gaufr�, � fleurs et arabesques toutes dor�es, mais ternies par le temps. 
� La dorure passe, le cuir reste. �, marmottaient les murailles.
Puis l'enfant fut conduit dans la chambre o� se tenait le vieux monsieur, qui l'accueillit avec un doux sourire, et lui dit : 
� Merci pour le soldat de plomb, mon petit ami ; et merci encore de ce que tu es venu me voir. �
Andersen.