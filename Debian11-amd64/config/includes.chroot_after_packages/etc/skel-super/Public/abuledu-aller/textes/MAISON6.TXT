Le soir, comme c'�tait l'usage dans le pays, une voiture tendue de noir s'arr�ta devant la porte ; on y pla�a un cercueil, qu'on devait porter bien loin, pour le mettre dans un caveau de famille. La voiture se mit en marche ; personne ne suivait que le vieux domestique; tous les amis du vieux monsieur �taient morts avant lui. 
Le petit gar�on pleurait, et il envoyait de la main des baisers d'adieu au cercueil. 
Quelques jours apr�s, la vieille maison fut pleine de monde, on y faisait la vente de tout ce qui s'y trouvait. Et, de la fen�tre, le petit gar�on vit partir, dans tous les sens, les chevaliers, les ch�telaines, les pots de fleurs en fa�ence, les fauteuils qui poussaient des knik-knak plus forts que jamais. Le portrait de la belle dame retourna chez le marchand de bric-�-brac; si vous voulez le voir, vous le trouverez encore chez lui; personne ne l'a achet�, personne n'y a fait attention. 
Au printemps, on d�molit la vieille maison. 
(Extrait de: La vieille maison d'Andersen)

