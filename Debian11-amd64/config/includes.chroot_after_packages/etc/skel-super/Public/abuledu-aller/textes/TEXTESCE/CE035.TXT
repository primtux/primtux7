Voracit� (1).
Mon fr�re Paul �tait un petit bonhomme de trois ans, la peau blanche, les joues rondes, avec de grands yeux d'un bleu tr�s clair, et les boucles dor�es de notre grand-p�re inconnu. Il �tait pensif, ne pleurait jamais, et jouait tout seul, sous une table, avec un bonbon ou un bigoudi.
Marcel Pagnol.