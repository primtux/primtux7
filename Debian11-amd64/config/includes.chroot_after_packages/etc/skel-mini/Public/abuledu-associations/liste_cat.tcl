############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : fichier.php
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: liste_cat.tcl,v 1.1 2006/05/21 10:15:29 david Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
#########################################################################
#!/bin/sh
#associations.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

###########################################
set ext .cat
set basedir [file dir $argv0]

set f [open [file join $basedir liste_cat.conf] "w"]

foreach i [lsort [glob [file join  $basedir categorie cat_a-lecture2 *.cat]]] {
	set categorie $i
	puts $f [lindex [split [file tail $i] .] 0]
	set g [open [file join $categorie] "r"]
	set listnom ""
	set listdata [gets $g]
	close $g
		foreach it $listdata {
		lappend listnom [lindex $it 1]
		}

	puts $f $listnom

}

close $f

