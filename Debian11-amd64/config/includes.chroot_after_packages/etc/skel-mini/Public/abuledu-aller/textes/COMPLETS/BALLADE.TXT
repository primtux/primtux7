Ballade � la lune.

C'�tait, dans la nuit brune, 
Sur le clocher jauni, 
La lune, 
Comme un point sur un i. 

Lune, quel esprit sombre
Prom�ne au bout d'un fil
Dans l'ombre, 
Ta face et ton profil ? 

Es-tu l' oeil du ciel borgne ? 
Quel ch�rubin cafard
Nous lorgne
Sous ton masque blafard ? 

N'es-tu rien qu'une boule ? 
Qu'un grand faucheux bien gras
Qui roule
Sans pattes et sans bras ? 

Es-tu, je t'en soup�onne, 
Le vieux cadran de fer
Qui sonne
L'heure aux damn�s d'enfer ? 

Sur ton front qui voyage, 
Ce soir ont-ils compt�
Quel �ge
A leur �ternit� ? 

Est-ce un ver qui te ronge
Quand ton disque noirci
S'allonge
En croissant r�tr�ci ? 

Qui t'avait �borgn�e 
L'autre nuit ? T'�tais-tu 
Cogn�e 
A quelque arbre pointu ? 

Car tu vins, p�le et morne, 
Coller sur mes carreaux
Ta corne, 
A travers les barreaux ... 

Alfred de Musset.
