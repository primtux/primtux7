On a march� sur la lune. 

Le vingt et un juillet mille neuf cent soixante -neuf, le r�ve se r�alise : devant des millions de t�l�spectateurs, les Am�ricains Armstrong et Aldrin posent le pied sur le sol de la Lune. Ils vont y planter le drapeau de leur pays, l'Am�rique, y recueillir des cailloux et y d�poser des appareils pour enregistrer les tremblements de Lune. 
Entre mille neuf cent soixante neuf et mille neuf cent soixante-douze, sept fus�es ont emmen� des astronautes am�ricains explorer la Lune. 
La mission Apollo 15 a �t� la premi�re � d�poser le Lunar Rover sur la Lune. Cette jeep lunaire pesait deux cents kilogrammes, parcourait une centaine de kilom�tres � dix-sept kilom�tres � l'heure. 
Les astronautes ont rapport� pr�s de quatre cents kilogrammes de cailloux et de poussi�res. 

