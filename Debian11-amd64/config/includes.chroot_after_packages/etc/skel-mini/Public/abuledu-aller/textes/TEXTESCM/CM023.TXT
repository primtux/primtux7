Le secret de ma�tre Cornille (9).
Alors, si on lui demandait d'o� diable pouvait venir tant d'ouvrage, il se mettait un doigt sur les l�vres et r�pondait gravement : 
� Motus ! je travaille pour l'exportation... � Jamais on n'en put tirer davantage. 
Quant � mettre le nez dans son moulin, il n'y fallait pas songer. La petite Vivette elle-m�me n'y entrait pas... Lorsqu'on passait devant, on voyait la porte toujours ferm�e, les grosses ailes toujours en mouvement, le vieil �ne broutant le gazon de la plate-forme, et un grand chat maigre qui prenait le soleil sur le rebord de la fen�tre et vous regardait d'un air m�chant.
Alphonse Daudet.