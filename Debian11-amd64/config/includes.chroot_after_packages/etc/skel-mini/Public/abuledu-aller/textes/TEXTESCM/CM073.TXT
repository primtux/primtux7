La vieille maison (2).
Toutes les autres maisons de la rue �taient neuves et belles � la mode r�gnante ; les carreaux de vitre �taient grands et toujours bien propres ; les murailles �taient lisses comme du marbre poli. Ces maisons se tenaient bien droites sur leurs fondations, et l'on voyait bien � leur air qu'elles n'entendaient rien avoir de commun avec cette construction des si�cles barbares. 
- N'est-il pas temps, se disaient-elles, qu'on d�molisse cette b�tisse surann�e, dont l'aspect doit scandaliser tous les amateurs du beau ? 
Andersen.