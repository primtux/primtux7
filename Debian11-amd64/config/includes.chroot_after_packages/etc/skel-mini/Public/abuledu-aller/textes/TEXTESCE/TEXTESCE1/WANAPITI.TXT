WANAPITI et sa dent de lait. 

Wanapiti arrivait � l'�ge o� les enfants perdent une � une toutes leurs dents... de lait. 
Elle interrogea son grand-p�re sur ce grand myst�re. Bec Royal lui expliqua que les esprits n'accordaient de belles dents aux petits indiens que s'ils �taient courageux, ob�issants et g�n�reux. 
Aussi, il lui conseilla d'offrir sa prochaine dent de lait � la personne qu'elle aimait le plus... 
L e soir m�me, Wanapiti sentit l'une de ses dents bouger. Elle la fit tomber d'un petit coup de langue et courut l'offrir � sa maman en lui disant : 
� Fleur de Montagne, tu es la personne que j'aime le plus au monde alors je t'offre ma dent de lait. �
Fleur de Montagne, tr�s touch�e, fit un petit trou dans la quenotte afin de la porter en pendentif comme un v�ritable bijou. 


