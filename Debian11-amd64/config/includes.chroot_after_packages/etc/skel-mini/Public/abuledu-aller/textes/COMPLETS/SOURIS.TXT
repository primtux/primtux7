Le chat et les souris.
Un certain chat �tait si habile � attraper les souris que celles-ci se trouvaient en danger continuel. A la fin, une nuit, la plus �g�e r�unit un conseil. Une � une, les souris racont�rent leurs ennuis et propos�rent un plan qui leur permettrait de vivre en paix. Mais aucun ne donnait satisfaction. Alors, un souriceau se leva et d�clara :
- Mon plan est tr�s simple : si le chat portait un grelot autour du cou, nous serions tenus au courant de chacun de ses mouvements, ce qui nous laisserait le temps de nous enfuir.
Et il s'assit, tr�s content de lui, au milieu des applaudissements. Alors, une vieille souris se leva � son tour, les yeux brillants de malice :
- Un plan excellent,dit-elle, mais... qui attachera le grelot au cou du chat ?