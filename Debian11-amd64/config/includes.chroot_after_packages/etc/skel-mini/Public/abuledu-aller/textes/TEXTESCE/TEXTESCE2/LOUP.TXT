Le loup et les chasseurs

Loup bleu est assis, immobile comme un rocher, ses pattes ant�rieures tendues et ses oreilles dress�es. 
- Ecoutez ! 
La bagarre cesse aussit�t. La neige retombe autour des louveteaux. D'abord, on n'entend rien. 
 Les rouquins ont beau dresser leurs oreilles fourr�es, il n'y a que la plainte soudaine du vent, comme un grand coup de langue glac�e. Et puis, tout d'un coup, derri�re le vent un hurlement de loup, tr�s long, qui raconte un tas de choses. 
- C'est Cousin Gris, murmure un des rouquins. 
- Qu'est-ce qu'il dit ? 
Flamme Noire jette un rapide coup d'oeil � Loup Bleu. L'un et l'autre savent bien ce que Cousin Gris leur dit, du haut de la colline o� il est plac� en sentinelle. 
L'Homme ! 
Une bande de chasseurs... 
Qui les cherchent. 
Les m�mes que la derni�re fois. 
- Fini de jouer, les enfants, pr�parez-vous, nous partons ! 

