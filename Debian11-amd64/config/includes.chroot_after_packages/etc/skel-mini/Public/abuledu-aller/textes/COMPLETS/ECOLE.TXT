La vieille �cole.

Pauvre vieille �cole, d�labr�e, malsaine, mais si amusante ! Ah ! les beaux b�timents qu'on construit ne te feront pas oublier. Les chambres du premier �tage, celles des instituteurs, �taient maussades et incommodes ; le rez-de-chauss�e, nos deux salles l'occupaient, la grande et la petite, deux salles incroyables de laideur et de salet�, avec des tables comme je n'en revis jamais, diminu�es de moiti� par l'usure, et sur lesquelles nous aurions d�, raisonnablement, devenir bossues au bout de six mois. L'odeur de ces classes, apr�s les trois heures d'�tude du matin et de l'apr�s-midi, �tait litt�ralement � renverser.

Colette.