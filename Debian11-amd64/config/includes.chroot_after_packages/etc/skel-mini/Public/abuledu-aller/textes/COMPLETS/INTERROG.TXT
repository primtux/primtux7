Interrogations.
Pas de lune. Les �toiles avaient au fond du ciel noir des scintillements fr�missants. Qui habite ces mondes ? Quelles formes, quels vivants, quels animaux, quelles plantes sont l�-bas ? Ceux qui pensent dans ces univers lointains, que savent-ils plus que nous ? Que peuvent-ils plus que nous ? Que voient-ils que nous ne connaissons point ? Un d'eux, un jour ou l'autre, traversant l'espace, n'appara�tra-t-il pas sur notre terre pour la conqu�rir, comme les Normands jadis traversaient la mer pour asservir des peuples plus faibles ? 
Guy de Maupassant.
