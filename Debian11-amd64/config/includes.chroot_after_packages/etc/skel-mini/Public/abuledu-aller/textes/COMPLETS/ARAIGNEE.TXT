L'araign�e.
Une belle araign�e des jardins, ma foi, le ventre en gousse d'ail, barr� d'une croix histori�e. Elle dormait ou chassait, le jour, sur sa toile tendue au plafond de la chambre � coucher. La nuit, vers trois heures, au moment o� l'insomnie quotidienne rallumait la lampe, rouvrait le livre au chevet de ma m�re, la grosse araign�e s'�veillait aussi, prenait ses mesures d'arpenteur et quittait le plafond au bout d'un fil, droit au-dessus de la veilleuse � huile o� ti�dissait, toute la nuit, un bol de chocolat. Elle descendait, lente, balanc�e mollement comme une grosse perle, empoignait de ses huit pattes le bord de la tasse, se penchait t�te premi�re, et buvait jusqu'� sati�t�. Puis elle remontait, lourde de chocolat cr�meux, avec les haltes, les m�ditations qu'imposent un ventre trop charg�, et reprenait sa place au centre de son gr�ement de soie... 
Colette.


