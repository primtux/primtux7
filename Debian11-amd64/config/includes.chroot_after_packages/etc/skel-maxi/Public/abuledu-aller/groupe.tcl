############################################################################
# Copyright (C) 2007 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : groupe.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: groupe.tcl,v 1.1.1.1 2004/04/16 11:45:45 erics Exp $
# @author     David Lucardi
# @project
# @copyright  David Lucardi
#
#
#########################################################################
#!/bin/sh
#Editeur.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

##################################"sourcing
global plateforme LogHome baseHome Home

source path.tcl
source fonts.tcl

set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
init $plateforme
inithome

initlog $plateforme $ident

source msg.tcl
. configure -background white

#################################################g�n�ration interface
proc interface {} {
global sysFont LogHome Home
catch {destroy .geneframe}
catch {destroy .leftframe}
frame .geneframe -background white -height 300 -width 400
pack .geneframe -side left -anchor n

frame .geneframe.frame1 -background white -width 100 
pack .geneframe.frame1 -side left -padx 10  
frame .geneframe.frame1.frame1top -background white -width 100 
pack .geneframe.frame1.frame1top -side top 
label .geneframe.frame1.frame1top.a1 -bg blue -foreground white -text [mc {Eleves}] -font $sysFont(s)
pack .geneframe.frame1.frame1top.a1 -side top -fill x -expand 1 -pady 5

frame .geneframe.frame1.frame1bottom -background white -width 100 
pack .geneframe.frame1.frame1bottom -side top

listbox .geneframe.frame1.frame1bottom.listeleve -yscrollcommand ".geneframe.frame1.frame1bottom.scrollpage set" -width 15 -height 6
scrollbar .geneframe.frame1.frame1bottom.scrollpage -command ".geneframe.frame1.frame1bottom.listeleve yview" -width 7
pack .geneframe.frame1.frame1bottom.listeleve .geneframe.frame1.frame1bottom.scrollpage -side left -fill y -expand 1
foreach i [lsort [glob [file join $LogHome *.log]]] {
.geneframe.frame1.frame1bottom.listeleve insert end [string map {.log \040} [file tail $i]]
}

frame .geneframe.frame11 -background white -width 100 
pack .geneframe.frame11 -side left -padx 10  
button .geneframe.frame11.butright1 -text " >> " -command "addeleve" -activebackground white
pack .geneframe.frame11.butright1 -padx 10 -pady 20 -side bottom
button .geneframe.frame11.butleft1 -text " << " -command "deleleve" -activebackground white
pack .geneframe.frame11.butleft1 -padx 10 -pady 20 -side bottom


frame .geneframe.frame2 -background white -width 100 
pack .geneframe.frame2 -side left 

frame .geneframe.frame2.frame2top -background white -width 100 
pack .geneframe.frame2.frame2top -side top 
label .geneframe.frame2.frame2top.a1 -bg blue -text [mc {Groupe}] -foreground white -font $sysFont(s)
pack .geneframe.frame2.frame2top.a1 -side top -fill x -expand 1 -pady 5

frame .geneframe.frame2.frame2bottom -background white -width 100 
pack .geneframe.frame2.frame2bottom -side top


listbox .geneframe.frame2.frame2bottom.listgroupe -yscrollcommand ".geneframe.frame2.frame2bottom.scrollpage set" -width 15 -height 6
scrollbar .geneframe.frame2.frame2bottom.scrollpage -command ".geneframe.frame2.frame2bottom.listgroupe yview" -width 7
pack .geneframe.frame2.frame2bottom.listgroupe .geneframe.frame2.frame2bottom.scrollpage -side left -fill y -expand 1
bind .geneframe.frame2.frame2bottom.listgroupe <ButtonRelease-1> "changelistgroupe %x %y"




frame .leftframe -background white -height 480 -width 100 
pack .leftframe -side left -anchor n

frame .leftframe.frame1 -background white -width 100 
pack .leftframe.frame1 -side left -padx 20


label .leftframe.frame1.lab2 -bg blue -foreground white -text [mc {Dossier de textes}] -font $sysFont(s)
pack .leftframe.frame1.lab2 -side top -fill x -expand 1 -pady 5

listbox .leftframe.frame1.listcat -yscrollcommand ".leftframe.frame1.scrollpage set" -width 20 -height 6
scrollbar .leftframe.frame1.scrollpage -command ".leftframe.frame1.listcat yview" -width 7
pack .leftframe.frame1.listcat .leftframe.frame1.scrollpage -side left -fill y -expand 1 -anchor n
bind .leftframe.frame1.listcat <ButtonRelease-1> "changecat %x %y"
catch {
foreach i [lsort [glob [file join $Home textes *]]] {
if {[file isdirectory $i]} {
.leftframe.frame1.listcat insert end [file tail $i]
}
}
}
frame .leftframe.frame2 -background white -width 100 
pack .leftframe.frame2 -side left 
label .leftframe.frame2.lab2 -bg blue -foreground white -text [mc {Texte}] -font $sysFont(s)
pack .leftframe.frame2.lab2 -side top -fill x -expand 1 -pady 5

listbox .leftframe.frame2.listtext -yscrollcommand ".leftframe.frame2.scrollpage set" -width 20 -height 6
scrollbar .leftframe.frame2.scrollpage -command ".leftframe.frame2.listtext yview" -width 7
pack .leftframe.frame2.listtext .leftframe.frame2.scrollpage -side left -fill y -expand 1 -anchor n
bind .leftframe.frame2.listtext <ButtonRelease-1> "changetext %x %y"

frame .leftframe.frame3 -background white -width 100
pack .leftframe.frame3 -side left 
button .leftframe.frame3.but0 -text [mc {Voir la fiche}] -command "montrefiche" -activebackground white -state disabled
pack .leftframe.frame3.but0 -side left -padx 10

}





###################################################################################"

proc montrefiche {} {
global LogHome progaide plateforme
set lstgrp ""
#set listexo \173closure\040[mc {Closure}]\040[mc {Complete}]\0401\0401\175\040\173reconstitution\040[mc {Reconstitution}]\040[mc {Complete}]\0401\0401\175\040\173phrase\040\173[mc {Phrases melangees}]\175\040\173[mc {Clique sur une phrase et deplace-la en cliquant sur les fleches}]\175\0401\0401\175\040\173mot\040\173[mc {Mots melanges}]\175\040\173[mc {Remets les mots en ordre en les deplacant sur les traits roses.}]\175\0401\0401\175\040\173faute\040\173[mc {Texte a corriger}]\175\040\173[mc {Clique sur les mots mal ecrits, reecris-les et appuie sur entree.}]\175\0401\0401\175\040\173espace\040\173[mc {Phrases sans espaces}]\175\040\173[mc {Clique pour separer les mots.}]\175\0401\0401\175\040\173incomplet\040\173[mc {Phrases incompletes}]\175\040\173[mc {Complete}]\175\0401\0401\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 1}]\175\040\173[mc {Exercice 1}]\175\0401\0400\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 2}]\175\040\173[mc {Exercice 2}]\175\0401\0400\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 3}]\175\040\173[mc {Exercice 3}]\175\0401\0400\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 4}]\175\040\173[mc {Exercice 4}]\175\0401\0400\175\040\173ponctuation1\040[mc {Ponctuation1}]\040\173[mc {Complete avec le signe de ponctuation convenable et appuie sur la touche entree pour valider.}]\175\0401\0401\175\040\173ponctuation2\040[mc {Ponctuation2}]\040\173[mc {Clique pour retablir la ponctuation et complete avec le signe convenable.}]\175\0401\0401\175
set listexo \173closure\040[mc {Closure}]\040[mc {Complete}]\0401\0401\175\040\173reconstitution\040[mc {Reconstitution}]\040[mc {Complete}]\0401\0401\175\040\173phrase\040\173[mc {Phrases melangees}]\175\040\173[mc {Clique sur une phrase et deplace-la en cliquant sur les fleches}]\175\0401\0401\175\040\173mot\040\173[mc {Mots melanges}]\175\040\173[mc {Remets les mots en ordre en les deplacant sur les traits roses.}]\175\0401\0401\175\040\173faute\040\173[mc {Texte a corriger}]\175\040\173[mc {Clique sur les mots mal ecrits, reecris-les et appuie sur entree.}]\175\0401\0401\175\040\173espace\040\173[mc {Phrases sans espaces}]\175\040\173[mc {Clique pour separer les mots.}]\175\0401\0401\175\040\173incomplet\040\173[mc {Phrases incompletes}]\175\040\173[mc {Complete}]\175\0401\0401\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 1}]\175\040\173[mc {Exercice 1}]\175\0401\0400\175\040\173\173[mc {Exercice supplementaire}]\175\040\173[mc {Exercice supplementaire 2}]\175\040\173[mc {Exercice 2}]\175\0401\0400\175\040\173\173[mc {Flash}]\175\040\173[mc {Flash}]\175\040\173[mc {Clique sur le mot convenable}]\175\0401\0401\175\040\173\173[mc {Rapido}]\175\040\173[mc {Rapido}]\175\040\173[mc {Clique sur le mot convenable}]\175\0401\0401\175\040\173ponctuation1\040[mc {Ponctuation1}]\040\173[mc {Complete avec le signe de ponctuation convenable et appuie sur la touche entree pour valider.}]\175\0401\0401\175\040\173ponctuation2\040[mc {Ponctuation2}]\040\173[mc {Clique pour retablir la ponctuation et complete avec le signe convenable.}]\175\0401\0401\175\040\173Dictee\040[mc {Dict�e}]\040\173[mc {Compl�te et appuie sur le bouton pouce.}]\175\0401\0401\175

	for {set k 0} {$k < [.geneframe.frame2.frame2bottom.listgroupe size]} {incr k 1} {
	lappend lstgrp [.geneframe.frame2.frame2bottom.listgroupe get $k]
	}
set dossier [.leftframe.frame1.listcat get active]
set texte [.leftframe.frame2.listtext get active]
set file [file join textes $dossier $texte]
       if [string match *.alr $file] {
	 set h [open [file join $file] "r" ] 
	 	while {![eof $h]} {
		set tmp [gets $h]
			if [string match "set listexo *" $tmp] {
			eval $tmp
			break
			}
		}
	  close $h
	}
set ext .log
###############################################"
#set nom [string trim [lindex $lstgrp 0]]
	foreach nome $lstgrp {
	set nom [string trim $nome]
regsub -all {[^A-Za-z]} $nom _ dnom	
set listeval0$dnom ""
	set listeval1$dnom ""
	set listeval2$dnom ""
 	set listmp ""

		if {[catch { set f [open [file join $LogHome $nom$ext] "r" ] }] } {
      	set answer [tk_messageBox -message [mc {Erreur de fichier}] -type ok -icon info]
      	exit
		}
		while {![eof $f]} {
		lappend listmp [gets $f]
		}
	close $f
	set date0 [clock format [clock scan now] -format "%d/%m/%Y"]
	set date1 [clock format [clock scan "-1 day"] -format "%d/%m/%Y"]
	set date2 [clock format [clock scan "-2 day"] -format "%d/%m/%Y"]
		set listdoublon0 ""
		set listdoublon1 ""
		set listdoublon2 ""
		for {set x [llength $listmp]} {$x>-1} {incr x -1} {
		set it [lindex $listmp $x]
		#foreach it $listmp
		set date [string trim [lindex [split [lindex $it 6] -] 0]]
			if {[lindex $it 1] == $texte && [lindex $it 8] ==$dossier} {
			if {$date ==$date0} {
			if {[lsearch $listdoublon0 [lindex $it 5]] == -1} {
			lappend listeval0$dnom $it
			lappend listdoublon0 [lindex $it 5]
			}
			} 
			if {$date ==$date1} {
			if {[lsearch $listdoublon1 [lindex $it 5]] == -1} {
			lappend listeval1$dnom $it
			lappend listdoublon1 [lindex $it 5]
			}
			}                          
			if {$date ==$date2} {
			if {[lsearch $listdoublon2 [lindex $it 5]] == -1} {
			lappend listeval2$dnom $it
			lappend listdoublon2 [lindex $it 5]
			}                          
 			}                        	
			}
		}
	}
set header "<head></head>"
if {$plateforme == "unix"} { set header "<head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'></head>"}
if {$plateforme == "windows"} { set header "<head><meta http-equiv='Content-Type' content='text/html; charset=ISO 8859-1'></head>"}
set g [open [file join $LogHome tmp3.htm] "w" ] 
puts $g  $header
puts $g  "<html><body>"
puts $g "<p align = center>[mc {Fiche de groupe}] - [mc {Exercice}] $texte - [mc {Score de reussite (%)}]</p>"
#############################################################""""""
puts $g "<p>Date : $date0</p>"
puts $g "<p></p>"
puts $g "<table border=1 ><tr>"
puts $g "<td></td>"
	for {set i 0} {$i <14} {incr i} {
	if {[lindex [lindex $listexo $i] 4] == 1} {
	puts $g "<td>[lindex [lindex $listexo $i] 1]</td>"
	}
	}
puts $g "</tr>"
	foreach nome $lstgrp {
	set nom [string trim $nome]
	puts $g "<tr><td>$nom</td>"
		for {set i 0} {$i <14} {incr i} {
		if {[lindex [lindex $listexo $i] 4] == 1} {

		set flag 0
			foreach item [expr \$listeval0$dnom] {
				if {[lindex $item 5] == $i} {
				puts $g "<td>[lindex $item 3]</td>"
				set flag 1
				}
			}
			if {$flag == 0} {
			puts $g "<td>&nbsp;</td>"
			}
		}
		}
	puts $g "</tr>"
	}
puts $g "</table>"

####################################################################"
#############################################################""""""
puts $g "<p>Date : $date1</p>"
puts $g "<p></p>"
puts $g "<table border=1 ><tr>"
puts $g "<td></td>"
	for {set i 0} {$i <14} {incr i} {
	if {[lindex [lindex $listexo $i] 4] == 1} {
	puts $g "<td>[lindex [lindex $listexo $i] 1]</td>"
	}
	}
puts $g "</tr>"
	foreach nome $lstgrp {
	set nom [string trim $nome]
	puts $g "<tr><td>$nom</td>"
		for {set i 0} {$i <14} {incr i} {
		if {[lindex [lindex $listexo $i] 4] == 1} {
		set flag 0
			foreach item [expr \$listeval1$dnom] {
				if {[lindex $item 5] == $i} {
				puts $g "<td>[lindex $item 3]</td>"
				set flag 1
				}
			}
			if {$flag == 0} {
			puts $g "<td>&nbsp;</td>"
			}
		}
		}
	puts $g "</tr>"
	}
puts $g "</table>"

####################################################################"
#############################################################""""""
puts $g "<p>Date : $date2</p>"
puts $g "<p></p>"
puts $g "<table border=1 ><tr>"
puts $g "<td></td>"
	for {set i 0} {$i <14} {incr i} {
	if {[lindex [lindex $listexo $i] 4] == 1} {
	puts $g "<td>[lindex [lindex $listexo $i] 1]</td>"
	}
	}
puts $g "</tr>"
	foreach nome $lstgrp {
	set nom [string trim $nome]
	puts $g "<tr><td>$nom</td>"
		for {set i 0} {$i <14} {incr i} {
		if {[lindex [lindex $listexo $i] 4] == 1} {
		set flag 0
			foreach item [expr \$listeval2$dnom] {
				if {[lindex $item 5] == $i} {
				puts $g "<td>[lindex $item 3]</td>"
				set flag 1
				}
			}
			if {$flag == 0} {
			puts $g "<td>&nbsp;</td>"
			}
		}
		}
	puts $g "</tr>"
	}
puts $g "</table>"

####################################################################"


###################################################################"
puts $g  "</body></html>"
close $g
set fichier file:///[string map {" " %20} [file join [pwd] $LogHome tmp3.htm]]
exec $progaide $fichier &
}

proc fin {} {
destroy .
}

#######################################################################

proc addeleve {} {
set item [.geneframe.frame1.frame1bottom.listeleve get active]
if {$item == ""} {return}
.geneframe.frame1.frame1bottom.listeleve delete [.geneframe.frame1.frame1bottom.listeleve index active]
.geneframe.frame2.frame2bottom.listgroupe insert end $item
testebutok
}

proc deleleve {} {
set item [.geneframe.frame2.frame2bottom.listgroupe get active]
if {$item == ""} {return}
.geneframe.frame2.frame2bottom.listgroupe delete [.geneframe.frame2.frame2bottom.listgroupe index active]
.geneframe.frame1.frame1bottom.listeleve insert end $item
testebutok
}





proc changecat {x y} {
global Home 
.leftframe.frame1.lab2 configure -text [.leftframe.frame1.listcat get @$x,$y]
catch {
.leftframe.frame2.listtext delete 0 end
foreach i [lsort [glob [file join $Home textes [.leftframe.frame1.listcat get @$x,$y] *]]] {
.leftframe.frame2.listtext insert end [file tail $i]
}
}
.leftframe.frame2.lab2 configure -text [.leftframe.frame2.listtext get 0]
testebutok
}

proc changetext {x y} {
.leftframe.frame2.lab2 configure -text [.leftframe.frame2.listtext get @$x,$y]
testebutok
}

proc changelistgroupe {x y} {
.geneframe.frame2.frame2top.a1 configure -text [.geneframe.frame2.frame2bottom.listgroupe get @$x,$y]
testebutok
}

proc testebutok {} {
if {[.geneframe.frame2.frame2bottom.listgroupe get active] !="" && [.leftframe.frame1.listcat get active] != "" && [.leftframe.frame2.listtext get active] !=""} {
.leftframe.frame3.but0 configure -state normal
} else {
.leftframe.frame3.but0 configure -state disabled
}
}
interface

