Le marchand de ballons. 

Il y a un homme qui ressemble aux autres hommes, mais il est bien plus riche. 
Il porte un tr�sor sur son dos. Il est le marchand de ballons. 
Il porte un arbre aux mille fruits, aux fruits de toutes les couleurs, comme il en pousse dans les pays chauds. 
les ballons sont les fruits et son arbre est un manche � balai. 
Il y a des ballons rouges pareils � d'�normes groseilles. 
Il y en a des jaunes qui doivent avoir un go�t de citron. 
il y en a de bleus comme les yeux bleus. Il y en a des verts. Il y en a de plusieurs couleurs, blancs coup�s de tranches de roses, qui font envie comme des bonbons acidul�s. Et les rouges, les jaunes, les bleus, les verts, et ceux de plusieurs couleurs, tremblent au vent comme une bonne gel�e de fruits. 
Lequel choisir ? 
Lequel va s'arrondir sous nos doigts �cart�s ? 
Lequel va nous offrir sa joue ferme et collante qui sent le caoutchouc etqu'on n'ose pas pincer ?.
