Ce n'est pas dommage qu'on fasse dispara�tre cette antique baraque, dirent les imb�ciles, et ils �taient nombreux comme partout. Et, pendant que les ma�ons donnaient des coups de pioche, qui fendaient le coeur du petit gar�on, on voyait, de la rue, pendre des lambeaux de la tapisserie en cuir dor�, et les tulipes volaient en �clats, et les trompettes tombaient par terre, lan�ant un dernier schnetterendeng. 
Enfin, on enleva tous les d�combres et on construisit une grande belle maison � larges fen�tres et � murailles bien lisses, proprement peintes en blanc. Par devant, on laissa un espace pour un gentil petit jardin qui, sur la rue, �tait entour� d'une jolie grille neuve : 
-Que tout cela a bonne fa�on ! disaient les voisins. Dans le jardin, il y avait des all�es bien droites, et des massifs bien ronds; les plantes �taient align�es au cordeau, et ne poussaient pas � tort et � travers. 
(Extrait de: La vieille maison d'Andersen)


