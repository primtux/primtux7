La vieille maison (15).
Enfin, on enleva tous les d�combres et on construisit une grande belle maison � larges fen�tres et � murailles bien lisses, proprement peintes en blanc. Par devant, on laissa un espace pour un gentil petit jardin qui, sur la rue, �tait entour� d'une jolie grille neuve : 
� Que tout cela a bonne fa�on ! �, disaient les voisins. Dans le jardin, il y avait des all�es bien droites, et des massifs bien ronds ; les plantes �taient align�es au cordeau, et ne poussaient pas � tort et � travers.
Andersen.