Les points de la coccinelle.

La coccinelle est malheureuse. Sa robe est rouge, toute rouge, du plus beau des rouges. Mais elle n'a pas de points. Et une coccinelle sans points noirs sur le dos, c'est comme un papillon sans ailes, un �l�phant sans trompe, un enfant sans nombril.
Une coccinelle a besoin de ses points pour porter bonheur. C'est comme �a.  
Alors la coccinelle se roule dans une fleur qui a le coeur tout jaune.
� J'aurai peut- �tre des points jaunes, se dit-elle, et �a vaut mieux que pas de points du tout.�
Mais quand elle sort de la fleur, sa robe est toute jaune, sans le moindre petit point. Elle s'envole, le vent souffle sur la poussi�re de fleur. La coccinelle est rouge de nouveau... Et toujours aussi malheureuse.
