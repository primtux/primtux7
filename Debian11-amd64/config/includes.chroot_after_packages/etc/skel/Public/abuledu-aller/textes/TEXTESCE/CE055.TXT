Un nichoir pour la m�sange (1).
- Est-ce que la m�sange reviendra faire son nid dans la bo�te � lettres, l'ann�e prochaine ? demande Vincent.
- J'esp�re que non, r�pond papa, ce n'est pas tr�s confortable.
- Dans les bois, o� les m�sanges font-elles leurs nids ?
- Habituellement, elles trouvent tr�s bien un endroit : un trou, un tronc d'arbre creux, une fente dans un mur...