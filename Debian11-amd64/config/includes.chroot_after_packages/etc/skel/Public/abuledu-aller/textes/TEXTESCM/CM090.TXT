La panth�re (2).
Cette tranquille et redoutable h�tesse ronflait dans une pose aussi gracieuse que celle d'une chatte couch�e sur le coussin d'une ottomane. 
Ses sanglantes pattes, nerveuses et bien arm�es, �taient en avant de sa t�te qui reposait dessus, et de laquelle partaient ces barbes rares et droites, semblables � des fils d'argent.
Honor� De Balzac.