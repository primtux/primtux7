Le soleil et le vent (2).
Puis le soleil se mit � briller.
- Comme le temps a chang� ! fit le voyageur en �tant son manteau.
- Gagn�, dit le soleil.
- Attends, r�pondit le vent.
Et il se mit � souffler, souffler.
- Le temps est compl�tement d�traqu� ! et l'homme remit son v�tement.
- Tu as vu ! s'�cria le vent, tu as oblig� le voyageur � enlever son manteau et moi � le lui faire remettre. Nous sommes aussi forts l'un que l'autre.
Et jamais plus ils ne se disput�rent.